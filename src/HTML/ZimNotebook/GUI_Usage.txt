Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2012-08-04T16:39:21-07:00

====== GUI Usage ======

Starting GSnap without any command line flags causes the GSnap main GUI window to launch. The GUI is useful when you are interested in analyzing, viewing, or manipulating an individual snapshot. With the GUI, you can:

	* Measure the mass-weighted velocity dispersion of an arbitrary combination of particle types.
	* Change the dimensions of the slit mask used in the velocity dispersion measurement.
	* Quickly determine the number of particles that contribute to a velocity dispersion measurement.
	* Arbitrarily rotate and shift the particles in the snapshot.
	* Automatically center the snapshot so that its center of mass coincides with the origin of the coordinate system.
	* View the snapshot from any direction.
	* Zoom in and view arbitrary locations within the snapshot.
	* Save modified snapshot files.
	* Save images of the current preview.
	* Measure distances between (or sizes of) objects.

The general GUI work flow proceeds as follows: 

	1. Click on **Settings** → **Select Types** or press ''Alt+T'' to open the type selection dialog.
	2. Check the desired particle types: 
	
{{./screenshot_2012-08-09-181847.png?height=200}}	
			
	3. Open the snapshot file by either clicking **Open** in the tool bar or using **File** → **Open**.
	4. Perform your desired task (see below for more information).
	5. If you wish to save the snapshot, first click **Update** and then click **Save** or type **Ctrl+S**. If you do not click the **Update** button, your changes will likely not be saved to the snapshot file. In order to prevent the user from accidentally over-writing the original snapshot, the suffix ''.edit'' is appended to the file name of the saved snapshot. If you would like to over-write the original snapshot, Click** Save** → **Save As…** and choose the appropriate file name from the file chooser dialog.
	6. Exit the program by clicking in the window manager's close button or **File** → **Exit**.


=== The Main window ===

{{./screenshot_2012-08-04-175355.png?width=300}}	
 
	* Hovering over items in the main window reveals pop-up hints. 
	* The lower third of the window displays an event log.
	* Clicking the **Center** button causes GSnap to compute the center of mass of the particles selected in step 1. The particles are then shifted so that the center of mass coincides with the origin of the snapshot's coordinate system.
	* Clicking **Update **causes all changes you have made to the snapshot to be saved to the in-memory version of the snapshot. The in-memory snapshot is the version of the snapshot that will be saved to the disk if you click **Save**. Thus, **Update** means "commit changes to memory, but not to disk."
	* Clicking **Auto** causes GSnap to automatically recompute the velocity dispersion whenever the size of the slit changes or the system is modified (i.e., rotated or shifted). It also updates the size of the rectangular slit mask used inthe velocity dispersion measurements.  This prevents you from needing to click the **Compute sigma **and** Set Slit** button repeatedly whenever something changes.
	* Changing the x, y, and z coordinates and clicking **Shift coords** shifts the system so that the coordinate point (x,y,z) becomes the new origin. In other words, this centers the snapshot on the point (x,y,z).
	* Changing the direction angles, theta and phi, and clicking **Rotate** rotates the system so that the (θ, φ) direction becomes the new positive z-axis. **Note:** GSnap tends to view the system from the +z direction, and velocity dispersion measurements are only made along the z-axis.
	* Changing the length, width, or alpha and clicking **Set Slit** sets the dimensions and position angle of the slit mask used when making velocity dispersion measurements. **Note:** The value of alpha should remain at zero if you are using the preview window or else your results are likely to be incorrect (This should be fixed in a future version of GSnap).  
	* Clicking **Clear All** resets x, y, z, theta, phi, length, width, and alpha to zero and clears all output fields. It does not remove the snapshot from memory nor revert changes that have already been made to the snapshot.
	* Clicking **Compute Sigma** will compute the mass-weighted line of sight velocity dispersion of all selected particle types. //The slit must be set and it must have non-zero width and height// in order for the **Compute Sigma** button to function. In addition to the velocity dispersion, the number of particles appearing in the slit, ''n_slit'', is reported.  
	* Clicking **Preview** opens the interactive preview window, described below.

=== The Preview window ===

 {{./screenshot_2012-08-04-175544.png?width=300}}	

	* The preview image brightness is normalized such that the brightest pixel in the image is pure white (R, G, B) = (255, 255, 255).
	* The snapshot preview can be arbitrarily rotated using the **theta** and **phi** sliders.
	* The size of the viewing box can be changed using the **zoom** slider.  
	* The current values of theta, phi, and the viewing box size are displayed at the top of the viewing box.
	* The **gamma** slider adjusts the scaling of the pixel brightness.
	* Clicking the **Save Image** button allows you to save an image of the current view.
	* When the cursor moves over the image preview area, it becomes a cross-hair (+). Left clicking on any point in the preview window immediately rotates the coordinate system such that the current viewing direction becomes the new z-axis. Then the x and y coordinates are shifted in order to move the selected point to the origin.
	* Specifying the slit dimensions in the Main GSnap window and then clicking **Set Slit** causes the slit to be displayed in the Preview window. 
	* Right clicking in the preview window reveals a context menu. The context menu also informs you of several keyboard shortcuts. For instance:
		* pressing the ''s'' key causes GSnap to compute the velocity dispersion (the result appears in the Main window).
		* ''Ctrl+I'' toggles the visibility of the status information at the top of the window.
		* ''Ctrl+C'' performs the same action as the **Center** button in the Main window.
		* ''Ctrl+Z'' makes the z-axis coincide with the current viewing direction. This is like setting theta and phi in the Main window and then clicking the Rotate button.
	* The slit can be used as a measuring tool, since the slit dimensions are adjustable and you have the ability to arbitrarily rotate, zoom, and shift the system.


Related: [[Command Line Usage]]
