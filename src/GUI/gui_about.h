/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the About window GUI.

    The Qt make system uses this header, along with the About.ui user interface
    specification file, to generate a source file called moc_gui_about.cpp
    which can then be compiled to create a Qt GUI window. */

#ifndef INC_GUI_ABOUT_H
#define INC_GUI_ABOUT_H

#include "Core/global.h"
#include "ui_About.h"

#include <QtGui>
#include <QObject>
#include <QDialog>


/*!  \brief  Creates the "About" window.

    The "About" window for showing basic information about GSnap. Most of the
    work is in the Ui_About class which is currently auto-generated from the
    About.ui file.*/
class gui_about : public QDialog, public Ui_About
{
    Q_OBJECT

public:

    gui_about(QWidget* parent) : QDialog(parent) { setupUi(this); }
};

#endif
