/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the type selection window GUI.

    See select.cc for more information.
*/


#ifndef INC_SELECT_H
#define INC_SELECT_H

#define UTF8 QString::fromUtf8

#include "Core/global.h"
#include "Core/particles.h"

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QDialog>
#include <QFrame>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QWidget>


/*! \brief Creates the particle type selection dialog.*/
class gui_select
{
public:
    QVBoxLayout* verticalLayout;
    QCheckBox*   Gas_CheckBox;
    QCheckBox*   DM_CheckBox;
    QCheckBox*   Disk_CheckBox;
    QCheckBox*   Bulge_CheckBox;
    QCheckBox*   New_stars_CheckBox;
    QCheckBox*   BH_CheckBox;
    QFrame*      line;
    QCheckBox*   All_stars_CheckBox;
    QCheckBox*   All_CheckBox;

    void setupUi(QDialog* Select)
    {
        if (Select->objectName().isEmpty())
            { Select->setObjectName(UTF8("Select")); }

        Select->resize(140, 270);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Select->sizePolicy().hasHeightForWidth());
        Select->setSizePolicy(sizePolicy);
        Select->setMinimumSize(QSize(120, 230));
        Select->setMaximumSize(QSize(140, 270));
        verticalLayout = new QVBoxLayout(Select);
        verticalLayout->setObjectName(UTF8("verticalLayout"));
        Gas_CheckBox = new QCheckBox(Select);
        Gas_CheckBox->setObjectName(UTF8("Gas_CheckBox"));

        verticalLayout->addWidget(Gas_CheckBox);

        DM_CheckBox = new QCheckBox(Select);
        DM_CheckBox->setObjectName(UTF8("DM_CheckBox"));

        verticalLayout->addWidget(DM_CheckBox);

        Disk_CheckBox = new QCheckBox(Select);
        Disk_CheckBox->setObjectName(UTF8("Disk_CheckBox"));
        Disk_CheckBox->setChecked(true);

        verticalLayout->addWidget(Disk_CheckBox);

        Bulge_CheckBox = new QCheckBox(Select);
        Bulge_CheckBox->setObjectName(UTF8("Bulge_CheckBox"));
        Bulge_CheckBox->setChecked(true);

        verticalLayout->addWidget(Bulge_CheckBox);

        New_stars_CheckBox = new QCheckBox(Select);
        New_stars_CheckBox->setObjectName(UTF8("New_stars_CheckBox"));
        New_stars_CheckBox->setChecked(true);

        verticalLayout->addWidget(New_stars_CheckBox);

        BH_CheckBox = new QCheckBox(Select);
        BH_CheckBox->setObjectName(UTF8("BH_CheckBox"));

        verticalLayout->addWidget(BH_CheckBox);

        line = new QFrame(Select);
        line->setObjectName(UTF8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        All_stars_CheckBox = new QCheckBox(Select);
        All_stars_CheckBox->setObjectName(UTF8("All_stars_CheckBox"));

        verticalLayout->addWidget(All_stars_CheckBox);

        All_CheckBox = new QCheckBox(Select);
        All_CheckBox->setObjectName(UTF8("All_CheckBox"));
        All_CheckBox->setChecked(true);

        verticalLayout->addWidget(All_CheckBox);

        QWidget::setTabOrder(Gas_CheckBox, DM_CheckBox);
        QWidget::setTabOrder(DM_CheckBox, Disk_CheckBox);
        QWidget::setTabOrder(Disk_CheckBox, Bulge_CheckBox);
        QWidget::setTabOrder(Bulge_CheckBox, New_stars_CheckBox);
        QWidget::setTabOrder(New_stars_CheckBox, BH_CheckBox);
        QWidget::setTabOrder(BH_CheckBox, All_stars_CheckBox);
        QWidget::setTabOrder(All_stars_CheckBox, All_CheckBox);

        retranslateUi(Select);
        QObject::connect(All_stars_CheckBox, SIGNAL(toggled(bool)),
                         All_CheckBox, SLOT(setChecked(bool)));
        QObject::connect(All_stars_CheckBox, SIGNAL(toggled(bool)),
                         DM_CheckBox, SLOT(setChecked(bool)));
        QObject::connect(All_stars_CheckBox, SIGNAL(toggled(bool)),
                         BH_CheckBox, SLOT(setChecked(bool)));
        QObject::connect(All_CheckBox, SIGNAL(toggled(bool)),
                         Disk_CheckBox, SLOT(setChecked(bool)));
        QObject::connect(All_CheckBox, SIGNAL(toggled(bool)),
                         Bulge_CheckBox, SLOT(setChecked(bool)));
        QObject::connect(All_CheckBox, SIGNAL(toggled(bool)),
                         New_stars_CheckBox, SLOT(setChecked(bool)));
        QObject::connect(All_stars_CheckBox, SIGNAL(toggled(bool)),
                         Gas_CheckBox, SLOT(setChecked(bool)));

        QMetaObject::connectSlotsByName(Select);
    } // setupUi

    void retranslateUi(QDialog* Select)
    {
        Select->setWindowTitle(QApplication::translate("Select",
                               "Types", 0, QApplication::UnicodeUTF8));
        Gas_CheckBox->setText(QApplication::translate("Select",
                              "Gas", 0, QApplication::UnicodeUTF8));
        DM_CheckBox->setText(QApplication::translate("Select",
                             "Dark Matter", 0, QApplication::UnicodeUTF8));
        Disk_CheckBox->setText(QApplication::translate("Select",
                               "Disk Stars", 0, QApplication::UnicodeUTF8));
        Bulge_CheckBox->setText(QApplication::translate("Select",
                                "Bulge Stars", 0, QApplication::UnicodeUTF8));
        New_stars_CheckBox->setText(QApplication::translate("Select",
                                    "New Stars", 0, QApplication::UnicodeUTF8));
        BH_CheckBox->setText(QApplication::translate("Select",
                             "Black Holes", 0, QApplication::UnicodeUTF8));
        All_stars_CheckBox->setText(QApplication::translate("Select",
                                    "All Particles", 0, QApplication::UnicodeUTF8));
        All_CheckBox->setText(QApplication::translate("Select",
                              "All Stars", 0, QApplication::UnicodeUTF8));
    } // retranslateUi
};


class selector : public QDialog, public gui_select
{
    Q_OBJECT

protected:

    types*   my_types;

public:

    selector(types* _my_types, QWidget* parent);

public slots:

    void Gas(bool b) { my_types->gas = b; }
    void DM(bool b) { my_types->dm = b; }
    void Disk(bool b) { my_types->disk = b; }
    void Bulge(bool b) { my_types->bulge = b; }
    void New_stars(bool b) { my_types->stars = b; }
    void BH(bool b) { my_types->bh = b; }

protected:

    void set_connections();
};

#endif // INC_SELECT_H
