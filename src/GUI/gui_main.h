/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the Main Application window GUI.

    The Qt make system uses this class, along with the GSnap.ui user interface
    specification file, to generate a source file called moc_gui_main.cpp which
    can then be compiled to create a Qt GUI window.

   \todo  Redesign to make the GUI look more clean. The preview window should be
   part of the main window and it should be implemented as a QGraphicsview
   Object. The galaxy image would then be in a QgraphicsScene.

   \todo  Add a progress bar for file loading / saving.
*/


#ifndef GUI_MAIN_H
#define GUI_MAIN_H

#include "ui_GSnap.h"
#include "gui_preview.h"
#include "gui_select.h"
#include "gui_about.h"
#include "gui_help_browser.h"
#include "Core/global.h"
#include "Core/particles.h"
#include "IO/io.h"

#include <QtGui>
#include <QObject>
#include <QFileDialog>


/*! \brief Creates GSnap's Main window.

   A class that connects the main GUI window to the a particle_type_group
   object so that particle_type_group objects can be used interactively */
class gui_main_window : public QMainWindow, public Ui_MainForm
{
    Q_OBJECT

protected:

    QApplication*   application;
    gadget_io*      gal;
    particle_type_group* my_particles;
    gui_preview*    preview_pointer;
    types           my_types;
    QString         file_name;
    QString         output_file;
    float           center_of_mass[7] = {0, 0, 0, 0, 0, 0, 0};
    double          x_shift = 0;
    double          y_shift = 0;
    double          z_shift = 0;
    double          width = 0;
    double          length = 0;
    double          theta = 0;
    double          phi = 0;
    double          alpha = 0;
    double          sigma = 0;
    bool            auto_update = false;
    bool            connections_set = false;
    bool            preview_window_open = false;

public slots:

    int  open_snapshot();
    int  save_as();
    void save_snapshot();
    void update();
    void preview();
    void set_x_shift(double x);
    void set_y_shift(double y);
    void set_z_shift(double z);
    void set_width(double w);
    void set_length(double l);
    void set_theta(double t);
    void set_slit_dims();
    void set_phi(double ph);
    void set_alpha(double a);
    void shift();
    void rotate();
    void compute_sigma();
    void zero();
    void set_auto_update(bool val);
    void center();
    void close();
    void preview_closed();
    void select_types();
    void show_about();
    void show_help_browser();

signals:

    void f_rotate(double r);
    void f_shift(double s);
    void f_set_alpha(double a);
    void f_sigma(QString s);
    void f_n_slit(QString n);
    void f_zero(double z);
    void f_zero_au(bool val);
    void f_center(QString c);

public:

    /*! The GUI constructor connects the signals and slots and initializes the
      particle_type_group object. */
    gui_main_window(QApplication* app,
                    particle_type_group* _my_particles,
                    QMainWindow* parent = NULL)
        : QMainWindow(parent)
    {
        my_particles = _my_particles;

        application = app;

        my_types.gas = my_types.dm = my_types.bh = 0;

        my_types.bulge = my_types.disk = my_types.stars = 1;

        setupUi(this);

        set_connections();
    }

    ~gui_main_window()
    {
        std::cerr << "\nGoodbye!\n";
    }

protected:

    /*! Set all connections needed to make this GUI work. */
    void set_connections()
    {
        // connect the spin boxes to their respective variables

        connect(xSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_x_shift(double)));

        connect(ySpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_y_shift(double)));

        connect(zSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_z_shift(double)));

        connect(thetaSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_theta(double)));

        connect(phiSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_phi(double)));

        connect(alphaSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_alpha(double)));

        connect(LengthSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_length(double)));

        connect(WidthSpinBox, SIGNAL(valueChanged(double)), this,
                SLOT(set_width(double)));

        // connect the button and action signals to their respective slots

        connect(ShiftButton, SIGNAL(clicked()), this,
                SLOT(shift()));

        connect(RotateButton, SIGNAL(clicked()), this,
                SLOT(rotate()));

        connect(SlitButton, SIGNAL(clicked()), this,
                SLOT(set_slit_dims()));

        connect(previewButton, SIGNAL(clicked()), this,
                SLOT(preview()));

        connect(actionUpdate, SIGNAL(triggered()), this,
                SLOT(update()));

        connect(CSigmaButton, SIGNAL(clicked()), this,
                SLOT(compute_sigma()));

        connect(actionSave, SIGNAL(triggered()), this,
                SLOT(save_snapshot()));

        connect(actionAuto, SIGNAL(toggled(bool)), this,
                SLOT(set_auto_update(bool)));

        connect(actionExit, SIGNAL(triggered()), application,
                SLOT(closeAllWindows()));

        connect(actionCenter, SIGNAL(triggered()), this,
                SLOT(center()));

        connect(actionClose_Snapshot, SIGNAL(triggered()), this,
                SLOT(close()));

        connect(actionSave_As, SIGNAL(triggered()), this,
                SLOT(save_as()));

        connect(actionOpen_Snapshot, SIGNAL(triggered()), this,
                SLOT(open_snapshot()));

        connect(actionSelect, SIGNAL(triggered()), this,
                SLOT(select_types()));

        connect(actionAbout, SIGNAL(triggered()), this,
                SLOT(show_about()));

        connect(actionHelp, SIGNAL(triggered()), this,
                SLOT(show_help_browser()));

        // reset particle manipulation spinboxes to prevent double click errors

        connect(this, SIGNAL(f_shift(double)), xSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_shift(double)), ySpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_shift(double)), zSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_rotate(double)), thetaSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_rotate(double)), phiSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_set_alpha(double)), alphaSpinBox,
                SLOT(setValue(double)));

        // display results of computations

        connect(this, SIGNAL(f_sigma(QString)), displaysigma,
                SLOT(setText(QString)));

        connect(this, SIGNAL(f_n_slit(QString)), display_n_slit,
                SLOT(setText(QString)));

        connect(this, SIGNAL(f_center(QString)), statusbar,
                SLOT(showMessage(QString)));

        // set up the "Clear All" button

        connect(ZeroButton, SIGNAL(clicked()), this,
                SLOT(zero()));

        connect(this, SIGNAL(f_zero(double)), phiSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero(double)), thetaSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero(double)), xSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero(double)), ySpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero(double)), zSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero(double)), WidthSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero(double)), LengthSpinBox,
                SLOT(setValue(double)));

        connect(this, SIGNAL(f_zero_au(bool)), actionAuto,
                SLOT(setChecked(bool)));
    }
};

#endif // GUI_MAIN_H
