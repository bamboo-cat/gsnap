/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Performs the depth of field effect.
*/

#ifndef DEPTH_OF_FIELD_H
#define DEPTH_OF_FIELD_H

#include "Core/global.h"
#include "Viz/voxel.h"

#include <vector>


/*! \brief Performs the depth of field effect.

    Performs a slice-based depth of field effect, whereby a fraction of the
    central region of the viewing volume is in focus, but the foreground
    and background are progressively more blurred with increasing distance from
    the center of the volume. Gaussian blurring is performed on each depth slice
    to achieve the desired effect.
*/
class depth_of_field
{
private:

    typedef std::vector<float> kernel;

    float  fractional_depth; // fraction of the volume that is in focus.
    pix    total_depth;      // total voxel depth of the volume.
    pix    near_cut;         // index of blur-focus boundary nearest the camera
    pix    far_cut;          // index of blur-focus boundary opposite the camera
    float  gradient;         // the "blur gradient"
    uint   max_blur;         // sets the maximum amount of blurring
    pix    xres;             // x-resolution
    pix    yres;             // y-resolution
    uint   npixels;          // number of pixels in each slice.
    bool   effect_enabled;   // specifies whether the blurring effect is enabled
    std::vector<kernel> kernels; // contains a set of blurring kernel signatures

public:

    /*! All member quantities are initialized to zero.*/
    depth_of_field();

    /*! Calls the init() method upon construction.
        \param _fractional_depth the fractional depth of field. A value of
        0.5 would indicate that the middle 50% of the viewing depth is in focus.
        \param _total_depth indicates the total depth of the viewing volume in
        units of pixel width (i.e. voxel depth).
        \param _xres x-resolution of the resulting image.
        \param _yres y-resolution of the resulting image.
        \see init(). */
    depth_of_field(float _fractional_depth,
                   pix _total_depth,
                   pix _xres,
                   pix _yres);

    /*! Initializes the object by computing basic constants and allocating
        memory.
        \param _fractional_depth the fractional depth of field. A value of
        0.5 would indicate that the middle 50% of the viewing depth is in focus.
        \param _total_depth indicates the total depth of the viewing volume in
        units of pixel width (i.e. voxel depth).
        \param _xres x-resolution of the resulting image.
        \param _yres y-resolution of the resulting image. */
    void init(float _fractional_depth,
              pix _total_depth,
              pix _xres,
              pix _yres);

    /*! \return true if depth of field effect can be performed, false otherwise.
        For example, if the init() member has not yet been called or the
        blurring effect is not practical for some reason, then this returns
        false. */
    bool enabled() { return effect_enabled; }

    /*! Determines the level of blurring for the kth slice, then determines
        whether the slice is empty or not. If the slice is not empty, it is
        blurred by the appropriate amount. \param voxel_slice is an array of
        voxels representing a depth slice. The array length must be at least
        2 * xres * yres. It is assumed that the first half of the array
        initially contains the voxel data. \param k is the index (depth) of the
        slice in the volume. \return 1 if the slice was blurred, 0 if the slice
        was not blurred. */
    int blur(voxel* voxel_slice, pix k);

private:

    /*! Perform a blur operation on a depth slice. \param blur_level
        determines the magnitude or "spread size" of the blur; higher values
        indicate more blurring. \param voxel_slice is an array of voxels
        corresponding to a plane of constant depth within the volume.*/
    int blur_slice(voxel* slice_buf,
                   uint blur_level) __attribute__((optimize("unroll-loops")));

    /*!
     * \brief Computes linear Gaussian kernels used for Gaussian blurring.
     */
    void compute_kernels();

};

#endif // DEPTH_OF_FIELD_H
