/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the voxel_array class.

    Stores an array of voxels (volume elements) for the purpose of volume
    rendering. Particle information is transformed into spatial density using an
    SPH smoothing kernel. The density can then be projected onto a pixel array
    for viewing purposes.

    \todo make a list of empty cvoxel depth slices. Store in an array and check
          the entries in this array at the beginning of the populate_voxel_slice
          step. In this way, entire slices can be skipped if they are empty.

 ******************************************************************************/


#ifndef VOXEL_ARRAY_H
#define VOXEL_ARRAY_H

#include "Core/global.h"
#include "Viz/pixel_array.h"
#include "Viz/image.h"
#include "Viz/voxel.h"

#include <QVector>

class compact_sph;
class color_particle;


/*! \brief Performs volume rendering on particle data using a Gaussian smoothing
    kernel.

   This is a voxel array class used for volume rendering of SPH data.
   The rendering is done using an SPH "gather" algorithm: We first create
   cubical voxels throughout the computational domain. Then, for each voxel,
   we compute the local density by computing the sum
   \f[
                      \rho = \sum_i m_i w(r_i , h)
   \f]
   where \f$m_i\f$ is the mass of the \f$i\f$th neighboring SPH particle,
   \f$r_i\f$ is the distance from the center of the voxel to the ith particle,
   \f$h\f$ is the smoothing length, and \f$w\f$ is a weight function.  For the
   weight function, we use the pseudo-Gaussian function:
   \f[
                    w(r, h) = \frac{105}{32\pi h^7}(h-r)^2(h+r)^2
   \f]
   The smoothing length, \f$h\f$, is scaled so that at least N_MIN and at most
   N_MAX particles are located within a distance \f$h\f$ of each voxel. In
   regions that are very sparse, we allow N_MIN to be a soft limit; arbitrarily
   small numbers of neighbors are allowed.

   Currently, the nearest neighbor particle search is accelerated by first
   constructing a coarse mesh which partitions the domain into smaller
   autonomous regions that can then be searched efficiently using the
   trivial brute force method.
*/
class voxel_array: public pixel_array
{

public: // types and comparison functors

    // coarse voxels
    struct cvoxel
    {
        // number of sph particles in this voxel.
        uint                    Nsph;

        // number of stellar particles in this voxel.
        uint                    Nstar;

        // stores the SPH particle info.
        QVector<compact_sph>    sph_parts;

        // stores stellar particle info.
        QVector<color_particle> star_parts;

        // temporary vector for storing neighbor info.
        QVector<uint>           contributors;
    };


    struct distnc
    {
        uint r2;
        uint id;
        uint N;
    };


    struct psf
    {
        ulong vox_idx;
        float red;
        float blue;
    };


    /*! \brief A structure that should fit into at most three cache lines
        on the most popular processors.

        This structure tries to guide the compiler's memory optimization by
        keeping these quantities local in memory because they are often used
        together.*/
    struct cache
    {
        uint  n;    // number of particles within radius h of the voxel center
        uint  nmin; // minimum number of neighbors
        uint  nmax; // maximum number of neighbors
        uint  idx;  // x_res*j+i
        float hmin; // the current lower bound on h
        float h;    // the smoothing length of this voxel
        float hmax; // the current upper bound on h
        float bluep;// blue luminosity of a particle
        float redp; // red luminosity of a particle
        float mp;   // particle mass
        float xp;   // x-position of a particle
        float x;    // x-position of a voxel-center
        float yp;   // y-position of a particle
        float y;    // y-position of a voxel-center
        float zp;   // z-position of a particle
        float z;    // z-position of a voxel-center
        float x2;   // (x-xp)^2
        float y2;   // (y-yp)^2
        float z2;   // (z-zp)^2
        float r2;   // distance between the voxel center and particle, squared
        float h2;   // the smoothing length squared
        float h7;   // 1.044/h^7
        float m;    // (h-r)*(r+h)
        float M;    // the mass density of this voxel
        float Red;  // the red density of this voxel
        float Blue; // the blue value of this voxel
        float temperature;
        char  tries;
    };


    /*! \brief A comparator, used for ordering distnc and psfstar objects.

        The object is used as an input to std::sort() while the class name is
        an input to std::priority_queue<>. The () operator is overloaded twice
        to handle the two different cases. */
    struct comp
    {
        bool operator() (const distnc& a, const distnc& b) const
        {
            return (a.r2 > b.r2);
        }

        bool operator() (const psf& a, const psf& b) const
        {
            return ( a.vox_idx > b.vox_idx );
        }
    } comparator;

protected:

    voxel*       voxels;
    cvoxel*      cvoxels;
    image*       img;
    pixel_array* red_pixels;
    pixel_array* blue_pixels;
    QVector<psf> psfs;

    kpc     voxel_size;
    float   ivoxel_size;
    kpc     cvoxel_size;
    float   icvoxel_size;
    kpc     view_depth;  // The depth of the viewing volume.
    kpc     camera_dist; // Distance between the camera and the origin.
    rad     alpha;
    uint    coarse_fine; // The ratio of cvoxel width to fine voxel width.
    pix     cxresolution; // Number of cvoxels along the width of the volume.
    pix     cyresolution; // Number of cvoxels along the height of the volume.
    pix     voxel_depth;
    pix     cvoxel_depth;
    uint    n_cvoxels;
    uint    cx_shift;
    uint    cy_shift;
    pix     z_shift;
    uint    cz_shift;
    bool    cvoxels_initialized;
    bool    perspective;  // if true, perspective projection is used.
    bool    red_pixels_initialized;
    bool    blue_pixels_initialized;
    enum    subtask {compute_light, compute_gas, compute_psf};

public:

    /*! parameterless, deferred initialization constructor. */
    voxel_array();

    /*! parameterized constructor. */
    voxel_array(image* __restrict _img);

    /*! destructor */
    ~voxel_array();

    /*! perform all initialization tasks. */
    void init();

    /*! free memory arrays allocated by the voxel_array object. */
    void free();

    /*! Set all voxel contents to zero. */
    void clear();

    /*! Add an sph object to the volume. This MUST be wrapped in a selection
        statement so that we never try to add particles to the mesh that
        are outside of the mesh's domain! */
    void add_particle(const compact_sph& p);

    /*! Add a stellar object to the volume. This MUST be wrapped in a selection
        statement so that we never try to add particles to the mesh that
        are outside of the mesh's domain!*/
    void add_particle(const color_particle& p);

    /*! Compactify the cvoxels array by resizing the cvoxels[i].parts vectors.*/
    void compact();

    /*! Perform the same tasks as vox_to_pix() all in one step rather than
        performing the steps separately. This requires us to store only one
        constant-depth slice of the volume at any given time rather than storing
        the entire volume in memory. The argument, kappa, determines the
        attenuation coefficient used in the opacity calculation.*/
    void slice_based_render(const float kappa);

    /*! Transfer data from the particle_type_group object to a voxel_array
     *  object.*/
    friend void particle_type_group::to_voxel_array(voxel_array* __restrict volume);

    /*! Retrieve the red value of the pixel at coordinates _i, _j */
    float get_red_pixel(const uint _i, const uint _j) const;

    /*! Retrieve the blue value of the pixel at coordinates _i, _j */
    float get_blue_pixel(const uint _i, const uint _j) const;

    /*!
     * \brief Get the normalized temperature of a pixel.
     * \param _i pixel index coordinate, measured from the left of the array.
     * \param _j pixel index coordinate, measured from the top of the array.
     * \return The normalized temperature of pixel (_i, _j). If the array has
     * not been normalized, an error is reported and GSnap exits. Thus, the
     * user must call voxel_array::normalize_temperature() before using this.
     */
    float get_temperature(const uint _i, const uint _j) const;

    /*!
     * \brief normalizes the temperature array
     *
     * Normalizes the temperature array to 1.0 so that the color mapping will
     * work properly.
     *
     * \see color_map
     */
    void normalize_temperature();

    /*! Clip and normalize the red and blue pixel arrays, using 1.0/max_val as
        the normalization factor. After applying the normalization, the image is
        clipped such that pixels brighter than max_value are set to max_value
        and pixels dimmer than min_value are set to min_value. This operation is
        usually followed by a gamma correction. */
    void color_clip(const float min_val = 0,
                    const float max_val = 0.8,
                    const float max_normal = 1);

    /*! Performs a gamma correction on the red and blue pixel_arrays. */
    void color_gamma_correction(const float gamma, const float beta);

    /*! Sets all pixels in the red pixel array to _red and all pixels in the
        blue pixel array to _blue. */
    void color_fill(const float _red, const float _blue);

protected:

    /*! Fill the coarse voxels with particle data. The computational domain
        is subdivided into cubical regions containing particles and voxels.
        Partitioning the volume in this way accelerates the nearest neighbor
        search when performing the SPH algorithm without the need for a more
        complicated data structure like a ball-tree. */
    void process_cvoxels(const subtask task);

    /*! Populate voxels one slice at a time using the SPH smoothing kernel. The
        index, k, specifies which depth slice will be computed. The array
        pointed to by voxel_slice stores the slice data. This would likely
        benefit from a CUDA implementation, since it is the most time-consuming
        and inefficient step in the volume rendering scheme.*/
    int populate_voxel_slice(const pix k, voxel* const voxel_slice);

    /*! The main work performed by the populate_voxel_slice is actually
        performed by this procedure. Calling with task == compute_gas computes
        the gas density in each voxel; task == compute_light computes the red
        and blue emission of each voxel in this slice.*/
    void compute_voxels(const subtask task,
                        const float z,
                        voxel* __restrict const voxel_slice);

    /*! Rather than using the volume rendering algorithm to visualize the stars,
        render stars as points that are slightly blurred with a point-spread
        function. */
    void render_with_psfs(const pix k, voxel* __restrict const voxel_slice );

    inline void update_voxel(const subtask task,
                             cache& cl,
                             voxel* vox);
};

#endif
