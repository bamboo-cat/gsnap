/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file

    \brief Member function definitions for the voxel_array class.

 ******************************************************************************/

#include "Viz/voxel_array.h"
#include "Viz/depth_of_field.h"
#include "Core/particles.h"

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <algorithm>
#include <omp.h>

#pragma GCC target ("fpmath=387")

using namespace std;


voxel_array::voxel_array()
{
    cvoxels_initialized = false;
    red_pixels_initialized = false;
    blue_pixels_initialized = false;
}


voxel_array::voxel_array(image* __restrict _img)
    : pixel_array(_img->ImageWidth, _img->ImageHeight, _img->ViewWidth)
{
    img = _img;
    voxel_array::init();
    camera_dist = 1 * view_size; // this needs to be an input value
    perspective = true;        // this should be a command line switch
}


voxel_array::~voxel_array()
{
    free();
}


void voxel_array::init()
{
    view_depth = img->ViewDepth;

    // (coarse voxel width) / (regular voxel width)
    coarse_fine = COARSE_FINE;

    if (pixels == nullptr)
    {
        pixel_array::init(img->ImageWidth,
                          img->ImageHeight,
                          img->ViewWidth);
    }

    if (img->ImageHeight % coarse_fine || img->ImageWidth % coarse_fine)
    {
        string msg = "The dimensions of the image should be multiples of ";
        msg += QString::number(coarse_fine).toStdString();
        msg += " pixels for best results.\n";
        gsnap::print_warning(msg);
    }

    // the x-resolution of the coarse mesh.
    cxresolution = uint( ceil( float(x_res / coarse_fine) ) ) + 2;

    // the y-resolution of the coarse mesh.
    cyresolution = uint( ceil( float(y_res / coarse_fine) ) ) + 2;

    // the voxel and cvoxel sizes
    voxel_size = view_size / float(x_res);
    cvoxel_size = view_size / (cxresolution - 2);

    // the inverse voxel size
    ivoxel_size = ipixel_size;
    icvoxel_size = float(cxresolution - 2) / view_size;

    // the depth of the viewing volume in voxels
    voxel_depth  = uint( floor(view_depth * ivoxel_size) );
    cvoxel_depth = uint( floor(view_depth * icvoxel_size) ) + 2;

    // the integer number of voxel shifts needed to translate the system's
    // origin to the center of the volume
    z_shift  = uint( floor(0.5 * float(voxel_depth) ) );
    cx_shift  = uint( floor(0.5 * cxresolution) );
    cy_shift  = uint( floor(0.5 * cyresolution) );
    cz_shift = uint( floor(0.5 * cvoxel_depth) );

    // the number of coarse voxels
    n_cvoxels = cxresolution * cyresolution * cvoxel_depth;

    cvoxels = new cvoxel[n_cvoxels];

    cvoxels_initialized = true;

    red_pixels = new pixel_array(x_res, y_res, view_size);
    red_pixels_initialized = true;

    blue_pixels = new pixel_array(x_res, y_res, view_size);
    blue_pixels_initialized = true;

}


void voxel_array::free()
{
    if (cvoxels_initialized)
    {
        delete [] cvoxels;
        cvoxels_initialized = false;
    }
}


void voxel_array::clear()
{
    if (cvoxels_initialized)
    {
        for (uint i = 0; i < n_cvoxels; ++i)
            { cvoxels[i].sph_parts.clear(); }
    }

    fill(0);

    if (red_pixels_initialized)
    {
        red_pixels->fill(0);
    }

    if (blue_pixels_initialized)
    {
        blue_pixels->fill(0);
    }
}


void voxel_array::add_particle(const compact_sph& p)
{
    uint xi = cx_shift + floor(p.r[0] * icvoxel_size);
    uint yi = cy_shift + floor(p.r[1] * icvoxel_size);
    uint zi = cz_shift + floor(p.r[2] * icvoxel_size);

    uint idx = cvoxel_depth * (cxresolution * yi + xi) + zi;

    cvoxels[idx].sph_parts.push_back(p);
}


void voxel_array::add_particle(const color_particle& p)
{
    if (img->render_light)
    {
        const uint xi = cx_shift + floor(p.r[0] * icvoxel_size);
        const uint yi = cy_shift + floor(p.r[1] * icvoxel_size);
        const uint zi = cz_shift + floor(p.r[2] * icvoxel_size);

        const uint idx = cvoxel_depth * (cxresolution * yi + xi) + zi;

        cvoxels[idx].star_parts.push_back(p);
    }

    if (img->render_stars)
    {
        const ullint xi = ullint(floor(p.r[0] * ivoxel_size + double(x_shift)));
        const ullint yi = ullint(floor(p.r[1] * ivoxel_size + double(y_shift)));
        const ullint zi = ullint(floor(p.r[2] * ivoxel_size + double(z_shift)));

        psf star;

        star.vox_idx = ullint(npixels) * zi + ullint(x_res) * yi + xi;

        if (star.vox_idx < ullint(npixels) * ullint(voxel_depth) )
        {
            star.red = p.red;
            star.blue = p.blue;
            psfs.push_back(star);
        }
    }
}


void voxel_array::compact()
{
    for (uint i = 0; i < n_cvoxels; ++i)
    {
        cvoxels[i].sph_parts.squeeze();
        cvoxels[i].star_parts.squeeze();
    }
}


// It may be possible to combine the two passes into one pre-processing pass
// for both gas and stars.

void voxel_array::process_cvoxels(const subtask task)
{
    if (task == compute_gas)
    {
        cerr << "Pre-processing gas data...";

        for (uint i = 0; i < n_cvoxels; ++i)
        {
            cvoxels[i].Nsph = cvoxels[i].sph_parts.size();
        }
    }
    else if (task == compute_light)
    {
        cerr << "Pre-processing stellar data...";

        for (uint i = 0; i < n_cvoxels; ++i)
        {
            cvoxels[i].Nstar = cvoxels[i].star_parts.size();
        }
    }

    // Sweep over surrounding elements in the coarse_mesh array, identify which
    // surrounding cvoxels contain particles.

    const int initial_search_rad = INITIAL_SEARCH_RADIUS; //(3)

    const int max_search_rad = MAX_SEARCH_RADIUS; //(6)

    const uint imax = cxresolution - 1;
    const uint jmax = cyresolution - 1;
    const uint kmax = cvoxel_depth - 1;

    priority_queue<distnc, QVector<distnc>, comp> distance_q;

    #pragma omp parallel for private (distance_q) schedule(dynamic)

    for (int j = 1; uint(j) < jmax; ++j)
    {
        for (uint i = 1; i < imax; ++i)
        {
            for (uint k = 1; k < kmax; ++k)
            {
                const uint idx = cvoxel_depth * (cxresolution * j + i) + k;

                cvoxel* const current_cvoxel = &cvoxels[idx];

                const int x = i;
                const int y = j;
                const int z = k;

                bool finished = false;

                // search radius in units of cvoxel_size
                int search_rad = initial_search_rad;

                while (!finished)
                {
                    uint ip_min = max( x - search_rad, 0 );
                    uint ip_max = min( x + search_rad, int(cxresolution) );

                    uint jp_min = max( y - search_rad, 0 );
                    uint jp_max = min( y + search_rad, int(cyresolution) );

                    uint kp_min = max( z - search_rad, 0 );
                    uint kp_max = min( z + search_rad, int(cvoxel_depth) );

                    for (register uint jp = jp_min; jp < jp_max; ++jp)
                    {
                        for (register uint ip = ip_min; ip < ip_max; ++ip)
                        {
                            for (register uint kp = kp_min; kp < kp_max; ++kp)
                            {
                                distnc dst;

                                dst.id = cvoxel_depth * (cxresolution * jp + ip)
                                         + kp;

                                const cvoxel* const cv = &cvoxels[dst.id];

                                if ( (task == compute_gas) && (cv->Nsph > 0) )
                                {
                                    dst.N = cv->Nsph;
                                }
                                else if ( (task == compute_light) &&
                                          (cv->Nstar > 0) )
                                {
                                    dst.N = cv->Nstar;
                                }
                                else { continue; }

                                // compute the distances between cvoxel centers.

                                const int dx = int(ip) - x;
                                const int dy = int(jp) - y;
                                const int dz = int(kp) - z;

                                // The distance squared.
                                dst.r2 =  dx * dx + dy * dy + dz * dz;

                                // add to the priority queue

                                distance_q.push(dst);
                            }
                        }
                    }

                    // if the distance queue is empty, expand the search radius
                    // until we reach the maximum allowed radius.

                    if (distance_q.empty())
                    {
                        if (search_rad < max_search_rad)
                        {
                            ++search_rad;
                            continue;
                        }
                        else if (search_rad >= max_search_rad)
                        {
                            search_rad = initial_search_rad;
                            finished = true;
                            continue;
                        }
                    }

                    uint N_nb = 0; // number of neighbors

                    // always include the immediate neighbors in the list of
                    // contributors

                    while ( (distance_q.top().r2 <= 4) && !distance_q.empty() )
                    {
                        const distnc cv = distance_q.top();
                        N_nb += cv.N;
                        current_cvoxel->contributors.push_back(cv.id);
                        distance_q.pop();
                    }

                    const uint min_nb = OVER_MIN_FACTOR * img->MinNeighbors;

                    // if the total number of particles included in the region
                    // exceeds the threshold, we are done!

                    if (N_nb >= min_nb)
                    {
                        priority_queue<distnc, QVector<distnc>, comp> empty_q;
                        distance_q.swap(empty_q);

                        finished = true;
                        search_rad = initial_search_rad;
                        continue;
                    }

                    /* the total number of neighboring particles is less than
                     the minimum and the distance queue contains contributor
                     data, so add the contributers until we reach the required
                     number of neighboring particles or the queue is empty. */

                    while ( (N_nb < min_nb) && !distance_q.empty() )
                    {
                        const distnc cv = distance_q.top();
                        N_nb += cv.N;
                        current_cvoxel->contributors.push_back(cv.id);
                        distance_q.pop();
                    }

                    /* if the number of neighboring particles is not large
                    enough to finish properly and the maximum radius is not yet
                    reached, expand the radius. If the radius is reached, and
                    the absolute minimum number of particles exists in this
                    region, use these particles, otherwise consider this region
                    empty. */

                    if ( (N_nb < min_nb) && (search_rad < max_search_rad) )
                    {
                        current_cvoxel->contributors.resize(0);

                        priority_queue<distnc, QVector<distnc>, comp> empty_q;
                        distance_q.swap(empty_q);

                        ++search_rad;
                        continue;
                    }
                    else if ( (N_nb < min_nb) && (search_rad >= max_search_rad) )
                    {
                        if (N_nb < img->MinNeighbors)
                        {
                            current_cvoxel->contributors.resize(0);
                        }

                        priority_queue<distnc, QVector<distnc>, comp> empty_q;
                        distance_q.swap(empty_q);

                        finished = true;
                        search_rad = initial_search_rad;
                        continue;
                    }

                    finished = true; // Is it possible to arrive here?
                }
            }
        }
    }

    /*
    Make copies of the neighboring cvoxels' particles for faster access.
    This uses more memory, but it makes the subsequent memory accesses
    sequential, rather than random. The performance improvement is significant.
    */

    for (uint cidx = 0; cidx < n_cvoxels; ++cidx)
    {
        uint n_contributors = cvoxels[cidx].contributors.size();

        if (n_contributors == 0) { continue; }

        for (uint q = 0; q < n_contributors; ++q)
        {
            // The index of a neighboring cvoxel.
            uint ni = cvoxels[cidx].contributors[q];

            if (ni == cidx) { continue; } // don't list particles twice!

            cvoxel& cvni = cvoxels[ni];

            if (task == compute_gas)
            {
                // Number of particles in neighboring cvoxel with index ni.

                uint len = cvni.Nsph;

                for (uint part = 0; part < len; ++part)
                {
                    cvoxels[cidx].sph_parts.append( cvni.sph_parts[part] );
                }
            }
            else if (task == compute_light)
            {
                uint len = cvni.Nstar;

                for (uint part = 0; part < len; ++part)
                {
                    cvoxels[cidx].star_parts.append( cvni.star_parts[part] );
                }
            }
        }
    }

    // the "contributors" vectors need to be cleared.

    for (uint i = 0; i < n_cvoxels; ++i) { cvoxels[i].contributors.clear(); }

    // keep track of the empty cvoxel depth slices (constant k index)...

    cerr << " Done!\n";
}


// This is the most time-consuming part of the rendering process. It should
// be redesigned eventually.
void voxel_array::compute_voxels(const subtask task,
                                 const float z,
                                 voxel* __restrict const voxel_slice)
{

    // determine the parent cvoxel k index.

    const uint kc = cz_shift + floor(z * icvoxel_size);

    //for (uint i = 0; i < npixels; ++i) { voxel_slice[i].zero(); }

    #pragma omp parallel for schedule(dynamic, COARSE_FINE)

    for (int j = 0; uint(j) < y_res; ++j)
    {
        uint  i = 0;

        // the i, j indices of the coarse voxel.
        uint  ic, jc;

        cache cl; // all frequently-used data will be stored compactly.

        cl.nmin = img->MinNeighbors;
        cl.nmax = img->MaxNeighbors;
        cl.idx = x_res * j;
        cl.hmin = 0;
        cl.h = 1.75 * cvoxel_size;
        cl.hmax = 0;
        cl.h2 = cl.h * cl.h;
        cl.h7 = 1.044 / (cl.h2 * cl.h2 * cl.h2 * cl.h);

        // determine y coordinate of the center of the voxel.

        cl.y = ( float(j) - float(y_shift) + 0.5 ) * voxel_size;

        cl.z = z;

        // determine the parent cvoxel j index.

        jc = cy_shift  + floor(cl.y * icvoxel_size);

        cl.tries = 0;

        const float blur_factor = (task == compute_light) ? 1.2 : 1.15;

        while (i < x_res)
        {
            // a pointer to the current voxel. resolving the address here speeds
            // things up because we don't have to resolve the address repeatedly
            voxel* const current_voxel = &voxel_slice[cl.idx];

            cl.n = 0; // number of particles within radius h of the voxel center
            cl.M = 0; // the value of the density field at the voxel center
            cl.Red = 0;
            cl.Blue = 0;

            // determine the x coordinate of the center of the voxel.

            cl.x = (float(i) - float(x_shift) + 0.5) * voxel_size;

            // determine the parent cvoxel i index.

            ic = cx_shift  + floor(cl.x * icvoxel_size);

            // determine the index of the cvoxel

            const uint cidx = cvoxel_depth * ( cxresolution * jc + ic ) + kc;

            // a pointer to the current cvoxel.
            cvoxel* const current_cvoxel = &cvoxels[cidx];

            // skip forward if the next set of voxels is empty.

            const uint n_gas = current_cvoxel->sph_parts.size();
            const uint n_star = current_cvoxel->star_parts.size();

            if ( (task == compute_gas) && (n_gas < cl.nmin) )
            {
                voxel* vox = current_voxel;

                for (uint vi = 0; vi < coarse_fine; ++vi)
                {
                    vox->set_mass(0);
                    vox++;
                }

                i += coarse_fine;
                cl.idx += coarse_fine;
                continue;
            }
            else if ( (task == compute_light) && (n_star < cl.nmin) )
            {
                voxel* vox = current_voxel;

                for (uint vi = 0; vi < coarse_fine; ++vi)
                {
                    vox->set_blue(0);
                    vox->set_red(0);
                    vox++;
                }

                i += coarse_fine;
                cl.idx += coarse_fine;
                continue;
            }

            cl.temperature = 0;

            // sweep over all particles in the cvoxels.parts list. Count the
            // number of particles within radius h.

            if (task == compute_light)
            {
                for (register uint s = 0; s < n_star; ++s)
                {
                    const color_particle* current_part =
                        &current_cvoxel->star_parts[s];
                    cl.xp = current_part->r[0];
                    cl.yp = current_part->r[1];
                    cl.zp = current_part->r[2];
                    cl.bluep = current_part->blue;
                    cl.redp = current_part->red;
                    cl.x2 = cl.x - cl.xp;
                    cl.y2 = cl.y - cl.yp;
                    cl.z2 = cl.z - cl.zp;
                    cl.r2 = cl.x2 * cl.x2 + cl.y2 * cl.y2 + cl.z2 * cl.z2;

                    if (cl.r2 < cl.h2)
                    {
                        cl.m = cl.h2 - cl.r2; // (h-r)*(r+h)
                        const float h7m2 = cl.h7 * cl.m * cl.m;
                        cl.Blue += cl.bluep * h7m2;
                        cl.Red += cl.redp * h7m2;
                        ++cl.n;
                    }
                }
            }

            if (task == compute_gas)
            {
                for (register uint s = 0; s < n_gas; ++s)
                {
                    const compact_sph* current_part =
                        &current_cvoxel->sph_parts[s];
                    cl.xp = current_part->r[0];
                    cl.yp = current_part->r[1];
                    cl.zp = current_part->r[2];
                    cl.mp = current_part->m;
                    cl.x2 = cl.x - cl.xp;
                    cl.y2 = cl.y - cl.yp;
                    cl.z2 = cl.z - cl.zp;
                    cl.r2 = cl.x2 * cl.x2 + cl.y2 * cl.y2 + cl.z2 * cl.z2;

                    if (cl.r2 < cl.h2)
                    {
                        cl.m = cl.h2 - cl.r2; // (h-r)*(r+h)
                        const float mp_h7_m2 = cl.mp * cl.h7 * cl.m * cl.m;
                        cl.M += mp_h7_m2;
                        cl.temperature += mp_h7_m2 * current_part->u;
                        ++cl.n;
                    }
                }
            }

            // limit the number of iterations

            if (cl.tries > 12)
            {
                update_voxel(task, cl, current_voxel);
                ++i;
                continue;
            }

            /* if the number of particles overlapping this voxel is not in
            the desired range, use a bisection search to quickly find
            the appropriate smoothing length.
            */
            if ( (cl.h < blur_factor * voxel_size) && (cl.n > cl.nmax) )
            {
                /* At this point, the smoothing length would otherwise become
                smaller than a voxel width, leading to sub-voxel rendering.
                Doing sub-voxel rendering is wasteful and leads to ugly
                artifacts in some cases. Thus, we adjust the mass to
                correct the brightness and continue to the next voxel. The
                exponent, 0.25, was determined using trial and error testing.
                */

                const float adjust = float(cl.nmax) / float(cl.n);
                const float correction = pow(adjust, 0.25);

                if (task == compute_gas)
                {
                    cl.M *= correction;
                }
                else if (task == compute_light)
                {
                    cl.Blue *= correction;
                    cl.Red *= correction;
                }

                update_voxel(task, cl, current_voxel);
                ++i;
            }
            else if (cl.n < cl.nmin)
            {
                cl.hmin = cl.h;

                if (cl.hmax == 0)
                {
                    // Increase the smoothing length.
                    // Note: this factor strongly effects performance.
                    if (cl.n < 0.2 * cl.nmin)
                    {
                        cl.h *= 1.091;
                    }
                    else if (cl.n < 0.9 * cl.nmin)
                    {
                        cl.h *= 1.09;
                    }
                    else
                    {
                        cl.h *= 1.035;
                    }
                }
                else
                {
                    cl.h = 0.5 * (cl.hmin + cl.hmax);
                }

                cl.h2 = cl.h * cl.h;
                cl.h7 = 1.044 / (cl.h2 * cl.h2 * cl.h2 * cl.h);
                ++cl.tries;
            }
            else if ( (cl.n > cl.nmax) && (cl.h > blur_factor * voxel_size) ) //1.15
            {
                cl.hmax = cl.h;
                cl.h = 0.5 * (cl.hmin + cl.hmax);
                cl.h2 = cl.h * cl.h;
                cl.h7 = 1.044 / (cl.h2 * cl.h2 * cl.h2 * cl.h);
                ++cl.tries;
            }
            else
            {
                update_voxel(task, cl, current_voxel);
                ++i;
            }
        }
    }
}


void voxel_array::update_voxel(const subtask task, cache& cl, voxel* vox)
{
    if (task == compute_gas)
    {
        vox->set_mass(cl.M);
        vox->set_u( (cl.M > 0) ? cl.temperature / cl.M : 0.0 );
    }
    else if (task == compute_light)
    {
        vox->set_blue(cl.Blue);
        vox->set_red(cl.Red);
    }

    ++cl.idx;
    cl.hmin = cl.hmax = cl.tries = 0;
}


void voxel_array::render_with_psfs(const pix k, voxel* const voxel_slice)
{
    for (uint i = 0; i < npixels; ++i) { voxel_slice[i].set_color(0.0, 0.0); }

    const ullint min_idx = ullint(npixels) * ullint(k);

    const ullint max_idx = min_idx + ullint(npixels);

    // to avoid the static modifier, this could be added as a class member
    static QVector<psf>::const_iterator it = psfs.begin();

//    if (it->vox_idx > max_idx)
//    {
//        cerr << "\nProblem! \nk = " << k END;
//        cerr << "it->vox_idx = " << it->vox_idx END;
//        cerr << "max_idx = " << max_idx END;
//        abort();
//    }

    while (it->vox_idx >= min_idx && it < psfs.end() && it->vox_idx < max_idx)
    {
        uint kplane_idx = (it->vox_idx > min_idx) ? it->vox_idx - min_idx : 0;

        voxel_slice[kplane_idx].add_color(it->blue, it->red);

        ++it;
    }
}


int voxel_array::populate_voxel_slice(const pix k,
                                      voxel* __restrict const voxel_slice)
{
    // the z displacement from the surface of the voxel array to its center.
    const float fzshift = voxel_size * ( 0.5 - float(z_shift) );

    // determine the z coordinate of the center of the voxel.

    const float z = voxel_size * float(k) + fzshift;

    // compute gas densities

    compute_voxels(compute_gas, z, voxel_slice);

    // compute light emission

    if (img->render_light)
    {
        compute_voxels(compute_light, z, voxel_slice);
    }
    else if (img->render_stars)
    {
        render_with_psfs(k, voxel_slice);
    }

    return 0;
}


void voxel_array::slice_based_render(const float kappa)
{
    process_cvoxels(compute_gas);

    if (img->render_light) { process_cvoxels(compute_light); }

    if (img->render_stars) { sort(psfs.begin(), psfs.end(), comparator); }

    // create the depth of field object

    depth_of_field dof;

    if (img->DepthOfField < 1.0)
    {
        dof.init(img->DepthOfField, voxel_depth, x_res, y_res);
    }

    // allocate a voxel slice array. The fist half of the array stores the data.
    // The second half is used as a buffer by the depth_of_field object.

    voxel* voxel_slice = new voxel[2 * npixels];

    // allocate and initialize an array for storing the obscuring mass along
    // the line of sight

    float* obscuring_mass = new float[npixels];

    memset (obscuring_mass, 0, sizeof(float) * npixels);

    cerr << "computing voxel densities...";

    for (int k = voxel_depth - 1; k > -1; --k)
    {
        // determine voxel brightnesses

        populate_voxel_slice( pix(k), voxel_slice );

        // perform depth of field blurring

        if ( dof.enabled() ) { dof.blur(voxel_slice, pix(k) ); }

        // perform opacity calculation and project the current voxel slice onto
        // the pixel array

        if (img->render_light || img->render_stars) // if rendering the light...
        {
            #pragma omp parallel for schedule(dynamic)

            for (int j = 0; uint(j) < y_res; ++j)
            {
                for ( uint i = 0; i < x_res; ++i)
                {
                    uint idx = x_res * j + i;

                    const voxel* current_voxel = &voxel_slice[idx];

                    // the gas mass in the current voxel
                    const float mk = current_voxel->get_mass();

                    // the total mass of intervening gas along the line of sight
                    // in front of this voxel, but not including this voxel.
                    const float previous_m = obscuring_mass[idx];

                    // the running total
                    obscuring_mass[idx] = previous_m + mk;

                    // now allow this voxel to partially obscure itself by
                    // contributing to its own line of sight obscuring mass

                    const float m = previous_m + 0.15 * mk;

                    // flux contributed by the current voxel after attenuation
                    const float b = current_voxel->get_blue() * exp(-kappa * m);
                    const float r = current_voxel->get_red() * exp(-0.25 * kappa * m);

                    blue_pixels->collect_photons(i, j, b);
                    red_pixels->collect_photons(i, j, r);
                }
            }
        }
        else if (img->render_gas)
        {
            #pragma omp parallel for schedule(dynamic)

            for (int j = 0; uint(j) < y_res; ++j)
            {
                for ( uint i = 0; i < x_res; ++i)
                {
                    const uint idx = x_res * j + i;

                    const float mk = voxel_slice[idx].get_mass();

                    const float uk = voxel_slice[idx].get_u();

                    // the total mass of intervening gas along the line of sight
                    // in front of this voxel, but not including this voxel.
                    const float previous_m = obscuring_mass[idx];

                    // the running total
                    obscuring_mass[idx] = previous_m + mk;

                    // now allow this voxel to partially obscure itself by
                    // contributing to its own line of sight obscuring mass

                    const float m = previous_m + 0.15 * mk;

                    const float w = mk * exp(-kappa * m);

                    // pixels[] stores the mass data

                    pixels[idx] += w;

                    // using red_pixels to store temperature data.

                    red_pixels->collect_photons(i, j, mk * uk);
                }
            }
        }
    }

    if (img->render_gas) { normalize_temperature(); }

    cerr << "Done!\n";

    delete [] voxel_slice;
    delete [] obscuring_mass;
}


float voxel_array::get_blue_pixel(const uint _i, const uint _j) const
{
    return blue_pixels->get_pixel(_i, _j);
}


float voxel_array::get_red_pixel(const uint _i, const uint _j) const
{
    return red_pixels->get_pixel(_i, _j);
}


float voxel_array::get_temperature(const uint _i, const uint _j) const
{
    if (!red_pixels->is_normalized())
    {
        gsnap::print_error("before calling voxel_array::get_temperature(), "
                           "temperature must be normalized to 1.0", err::bug);
    }

    return red_pixels->get_pixel(_i, _j);

}



void voxel_array::color_clip(const float min_val,
                             const float max_val,
                             const float max_normal)
{
    red_pixels->clip(min_val, max_val, max_normal);

    blue_pixels->clip(min_val, max_val, max_normal);
}


void voxel_array::color_gamma_correction(const float gamma, const float beta)
{
    red_pixels->gamma_correction(gamma, beta);

    blue_pixels->gamma_correction(gamma, beta);
}


void voxel_array::color_fill(const float _red, const float _blue)
{
    if (red_pixels_initialized) { red_pixels->fill(_red); }

    if (blue_pixels_initialized) { blue_pixels->fill(_blue); }
}


void voxel_array::normalize_temperature()
{
    if (red_pixels_initialized)
    {
        red_pixels->clip(0.0, 1e4, 1.0);
    }
}
