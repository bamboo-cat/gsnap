/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Provides visualization driver function prototypes.
 *
 *  Functions for visualizing the snapshots (i.e., creating images). These
 *  functions manipulate pixel_array and voxel_array objects.
 */

#ifndef IMAGE_H
#define IMAGE_H

#include "Core/global.h"
#include "Core/particles.h"
#include "Viz/color_map.h"

#include <map>
#include <QImage>
#include <QColor>
#include <QString>
#include <QPixmap>

class particle_type_group;

/*! \brief Draws images (i.e., 2D representations of the 3D snapshot.)

    Stores image creation settings and draws images using various techniques.
    In its present form, the data is not encapsulated and there is no
    constructor. This means that the user must set all of the values
    appropriately, then call the draw() method in order to render an image. This
    should eventually be modified to be a more "proper" object.
*/
struct image
{
    char* filename              = nullptr;
    particle_type_group* pgroup = nullptr;
    QString ColorMap            = "";
    pix   ImageWidth            = IMAGE_WIDTH;
    pix   ImageHeight           = IMAGE_HEIGHT;
    kpc   ViewWidth             = VIEW_SIZE;
    kpc   ViewDepth             = VIEW_SIZE;
    kpc   center[3]             = {0, 0, 0};
    rad   theta                 = 0;
    rad   phi                   = 0;
    uint  MinNeighbors          = N_MIN_GAS;
    uint  MaxNeighbors          = N_MAX_GAS;
    float GasOpacity            = OPACITY;
    float GasGamma              = GAS_GAMMA;
    float GasBeta               = GAS_BETA;
    float GasClipMin            = GAS_CLIP_MIN;
    float GasClipMaxVal         = GAS_CLIP_MAX_VAL;
    float GasClipMaxNorm        = GAS_CLIP_MAX_NORM;
    float StarGamma             = STAR_GAMMA;
    float StarBeta              = STAR_BETA;
    float StarClipMin           = STAR_CLIP_MIN;
    float StarClipMaxVal        = STAR_CLIP_MAX_VAL;
    float StarClipMaxNorm       = STAR_CLIP_MAX_NORM;
    float DepthOfField          = 1;
    bool  render_gas            = false;
    bool  render_light          = false;
    bool  render_stars          = false;
    types my_types;

    /*! \brief loads a particle_type_group object.
     *  \param _pgroup an object containing the particles that will be
     *  visualized by this class. */
    void load(particle_type_group* _pgroup) {pgroup = _pgroup;}

    /*! Draws an image of the contents of the snapshot. This merely calls the
    appropriate rendering function, based on the values of render_gas and
    render_light. If both are false, the draw_image() member is called. If
    either render_gas or render_light are true, the vol_render() member is
    called. */
    void draw();

private:

    /*! Draws an image of the particles in the snapshot, viewed along the
    z-axis and save the image as a PNG file. Each particle contributes to only
    one pixel (i.e., this function does not perform volume rendering.). */
    void draw_image();

    /*! Creates an image of the snapshot contents using a volume rendering
    scheme. If render_gas is true, then only the gas will be rendered. If
    render_light is true, then the starlight will be rendered.*/
    void vol_render();

    /*! Set the metadata of a QImage object */
    void set_QImage_meta_data(QImage& qimage);
};

#endif
