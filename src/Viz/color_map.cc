/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Contains the color_map class implementation.
 */

#include "color_map.h"


color_map::color_map()
{
    use_external_map = false;
    external_map_file = "";
    init();
}


color_map::color_map(const float temperature, const float brightness)
{
    color_map();
    set_value(temperature, brightness);
}


void color_map::init()
{
    red = green = blue = 0;
    make_map();
}


void color_map::set_value(const float temperature, const float brightness)
{

    if (temperature > 1.0f)
    {
        std::cerr << "temperature = " << temperature END;
        gsnap::print_error("in colormap::set(), temperature must be "
                           "normalized to 1.0", err::bug );
    }

    uint index = 255.0 * pow(temperature, 0.144);

    if (index > 255) { index = 255; }

    red = brightness * map[index].red();
    green = brightness * map[index].green();
    blue = brightness * map[index].blue();
}


void color_map::use_colorbar(std::string filename)
{
    external_map_file = filename;
    use_external_map = true;
}


void color_map::make_map()
{
    constexpr float dc = 1.0 / 255.0;

    rgb<float> color;

    if (use_external_map)
    {
        QImage png_map(external_map_file.c_str());

        if (png_map.isNull())
        {
            gsnap::print_error("The colormap image could not be loaded",
                               err::io);
        }

        for (uint i = 0; i < 256; ++i)
        {
            QColor c = QColor::fromRgba( png_map.pixel(int(i), int(0)) );
            const float r = float( c.redF() );
            const float g = float( c.greenF() );
            const float b = float( c.blueF() );

            // normalize the color such that the brightest component = 1.0 while
            // preserving the r:g:b ratios.

            float max = (r > g) ? r : g;

            if (b > max) { max = b; }

            const float i_max = (max == 0) ? 0 : 1.0 / max;

            color(i_max * r, i_max * g, i_max * b);

            map[i] = color;
        }
    }
    else
    {
        for (int i = 0; i < 256; ++i)
        {
            const float blue_green = pow(dc * i, 1.66);

            color(1.0f, blue_green, blue_green);

            map[i] = color;
        }
    }
}
