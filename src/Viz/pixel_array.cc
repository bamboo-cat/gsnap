/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Member functions of the pixel array class.

   For more information, refer to pixel_array.h.
*/

#include "Viz/pixel_array.h"

#include <cmath>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>


using namespace std;


pixel_array::pixel_array(const pix _x_res,
                         const pix _y_res,
                         const kpc x_size)
{
    pixels = nullptr;
    init(_x_res, _y_res, x_size);
}


pixel_array::pixel_array()
{
    pixels = nullptr;
}


pixel_array::~pixel_array()
{
    free();
}


void pixel_array::init(const pix _x_res,
                       const pix _y_res,
                       const kpc x_size)
{
    x_res = _x_res;

    y_res = _y_res;

    npixels = x_res * y_res;

    x_shift = uint( floor(0.5 * float(x_res) ) );

    y_shift = uint( floor(0.5 * float(y_res) ) );

    view_size = x_size;

    ipixel_size = float(x_res) / view_size;

    aspect_ratio = float(y_res) / float (x_res);

    normalized = false;

    if (pixels == nullptr) { pixels = new float [npixels]; }

    // initialize with empty (black) pixels.
    fill(0);
}


void pixel_array::free()
{
    if (pixels != nullptr)
    {
        delete [] pixels;
        pixels = nullptr;
    }
}


void pixel_array::fill(const float fill_value)
{
    if (pixels != nullptr)
    {
        std::memset(pixels, int(fill_value), sizeof(float) * npixels);
    }
}


void pixel_array::set_view_size(const kpc x_dim)
{
    view_size = x_dim;

    ipixel_size = kpc(x_res) / x_dim;

    normalized = false;
}


void pixel_array::add_particle(const kpc x, const kpc y, const float mass)
{
    pix x_index, y_index;

    x_index = x_shift + floor(x * ipixel_size);

    y_index = y_shift + floor(y * ipixel_size);

    const uint index = x_res * y_index + x_index;

    if (index < npixels) { pixels[index] += mass; }
}


void pixel_array::set_pixel(const pix xi, const pix yi, const float value)
{
    const uint index = x_res * yi + xi;

    if (index < npixels) { pixels[x_res * yi + xi] = value; }
}


void pixel_array::collect_photons(const pix xi, const pix yi, const float value)
{
    const uint index = x_res * yi + xi;

    if (index < npixels) { pixels[index] += value; }
}


void pixel_array::normalize()
{
    float max_p = 0;

    for (uint i = 0; i < npixels; ++i)
    {
        max_p = (max_p < pixels[i]) ? pixels[i] : max_p;
    }

    max_value = max_p;

    const float max_inv = (max_p > 0) ? 1.0 / max_p : 1.0;

    #pragma omp parallel for

    for (int i = 0; uint(i) < npixels; ++i)
    {
        pixels[i] *= max_inv;
    }

    normalized = true;
}


void pixel_array::clip(const float black_out,
                       const float white_in,
                       const float white_out)
{
    const float imax = white_out / white_in;

    #pragma omp parallel for

    for (int i = 0; uint(i) < npixels; ++i)
    {
        pixels[i] *= imax;

        if (pixels[i] > white_out) { pixels[i] = white_out; }

        if ( (pixels[i] < black_out) && (pixels[i] > 0) )
        {
            pixels[i] = black_out;
        }
    }

    normalized = true;
}


bool pixel_array::is_normalized() const
{
    return normalized;
}


void pixel_array::rescale_value(const float _gamma)
{
    gamma = _gamma;

    float max_p;

    max_p = 0;

    for (size_t i = 0; i < npixels; ++i) { max_p = max(max_p, pixels[i]); }

    max_value = max_p;

    const float max_inv = 1.0 / max_value;

    normalized = true;

    #pragma omp parallel for

    for (int i = 0; uint(i) < npixels; ++i)
    {
        if (pixels[i]) { pixels[i] = pow(max_inv * pixels[i], _gamma); }
    }
}


void pixel_array::gamma_correction(const float _gamma, const float _beta)
{
    gamma = _gamma;

    const float inv = 1.0 / (1.0 - exp(-_beta));

    #pragma omp parallel for

    for (int i = 0; uint(i) < npixels; ++i)
    {
        if (pixels[i])
        {
            const float tmp = (1.0 - exp(-_beta * pixels[i])) * inv;
            pixels[i] = pow(tmp, _gamma);
        }
    }
}


float pixel_array::get_pixel(const pix xi, const pix yi) const
{
    const uint index = x_res * yi + xi;

    if (index < npixels)
    {
        return pixels[index];
    }
    else { return 0; }
}
