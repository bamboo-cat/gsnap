/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Implements the visualization functions.
 */

#include "Core/particles.h"
#include "Viz/image.h"
#include "Viz/voxel_array.h"
#include "Viz/color_map.h"

#include <cmath>
#include <iostream>
#include <string>
#include <QString>

using namespace std;

void image::draw()
{
    if (render_gas || render_light || render_stars) { vol_render(); }
    else { draw_image(); }
}


void image::draw_image()
{
    // some name aliases for brevity
    kpc   view_size = ViewWidth;
    kpc   width = ImageWidth;
    kpc   height = ImageHeight;
    float min = StarClipMin;
    float max = StarClipMaxVal;
    float norm = StarClipMaxNorm;
    float gamma = StarGamma;
    float beta = StarBeta;

    QColor color;

    // create a QImage object for I/O.

    QImage gal_image(width, height, QImage::Format_RGB32);

    // Create a pixel_array object

    pixel_array pgroup_image(width, height, view_size);

    // Fill the pixel_array with raw data.

    pgroup->rect_mesh(&pgroup_image);

    // Normalize the pixel_array to prepare it for transfer to the QImage

    pgroup_image.clip(min, max, norm);
    pgroup_image.gamma_correction(gamma, beta);

    // transfer data from the pixel array to the QImage object

    float grayscale;

    for (size_t j = 0; j < height; ++j)
    {
        for (size_t i = 0; i < width; ++i)
        {
            grayscale = pgroup_image.get_pixel(i, j);
            color.setRgbF(grayscale, grayscale, grayscale);
            gal_image.setPixel(i, j , color.rgb());
        }
    }

    set_QImage_meta_data(gal_image);

    QString f(filename);
    f = f + ".png";
    gal_image.save(f);
}


void image::vol_render()
{
    cerr << "Beginning volume rendering... \n";

    const float min =    (render_gas) ? GasClipMin     : StarClipMin;
    const float max =    (render_gas) ? GasClipMaxVal  : StarClipMaxVal;
    const float norm =   (render_gas) ? GasClipMaxNorm : StarClipMaxNorm;
    const float gamma =  (render_gas) ? GasGamma       : StarGamma;
    const float beta =   (render_gas) ? GasBeta        : StarBeta ;
    const float width =  ImageWidth;
    const float height = ImageHeight;

    string color_map_filename = ColorMap.toStdString();

    QColor color;

    // create the QImage object

    QImage gal_image(width, height, QImage::Format_RGB32);

    // Fill the image with black pixels

    gal_image.fill(0);

    // Create a voxel_array object

    voxel_array pgroup_image(this);

    // add particles to the voxel_array object

    pgroup->to_voxel_array(&pgroup_image);

    // project voxels onto the pixels

    pgroup_image.slice_based_render(GasOpacity);

    // Clip the brightest and dimmest pixels
    if (render_gas)
    {
        pgroup_image.clip(min, max, norm);
    }
    else if (render_light || render_stars)
    {
        pgroup_image.color_clip(min, max, norm);
    }

    // Perform brightness scaling

    if (render_gas)
    {
        pgroup_image.gamma_correction(gamma, beta);
    }
    else if (render_light || render_stars)
    {
        pgroup_image.color_gamma_correction(gamma, beta);
    }

    // Transfer pixel data from the voxel array to the QImage object

    float brightness, red, green, blue, temperature;

    color_map pix;

    if (color_map_filename != "") { pix.use_colorbar(color_map_filename); }

    pix.init();

    for (uint j = 0; j < height; ++j)
    {
        for (uint i = 0; i < width; ++i)
        {
            if (render_gas)
            {
                brightness  = pgroup_image.get_pixel(i, j);
                temperature = pgroup_image.get_red_pixel(i, j);

                if (brightness <= 0.00444) { brightness = 0.00444; }

                pix.set_value(temperature, brightness);
                color.setRgbF(pix.get_red(), pix.get_green(), pix.get_blue() );
                gal_image.setPixel(i, j, color.rgb());
            }
            else if (render_light || render_stars)
            {
                red = pgroup_image.get_red_pixel(i, j);
                blue = pgroup_image.get_blue_pixel(i, j);
                green = 0.5 * (blue + red);

                if (red < 0.006) { red = 0.006; }

                if (green < 0.006) { green = 0.006; }

                if (blue < 0.006) { blue = 0.006; }

                color.setRgbF(red, green, blue);
                gal_image.setPixel(i, j, color.rgb());
            }
        }
    }

    pgroup_image.color_fill(0, 0);

    set_QImage_meta_data(gal_image);

    QString f(filename);
    f = f + ".png";
    gal_image.save(f);

    cerr << "Volume rendering is finished!\n";
}


void image::set_QImage_meta_data(QImage& qimage)
{
    qimage.setText("theta",           QString::number(theta));
    qimage.setText("phi",             QString::number(phi));
    qimage.setText("x_center",        QString::number(center[0]));
    qimage.setText("y_center",        QString::number(center[1]));
    qimage.setText("z_center",        QString::number(center[2]));
    qimage.setText("ImageWidth",      QString::number(ImageWidth));
    qimage.setText("ImageHeight",     QString::number(ImageHeight));
    qimage.setText("ViewWidth",       QString::number(ViewWidth));
    qimage.setText("ViewDepth",       QString::number(ViewDepth));
    qimage.setText("MinNeighbors",    QString::number(MinNeighbors));
    qimage.setText("MaxNeighbors",    QString::number(MaxNeighbors));
    qimage.setText("GasOpacity",      QString::number(GasOpacity));
    qimage.setText("GasGamma",        QString::number(GasGamma));
    qimage.setText("GasBeta",         QString::number(GasBeta));
    qimage.setText("GasClipMin",      QString::number(GasClipMin));
    qimage.setText("GasClipMaxVal",   QString::number(GasClipMaxVal));
    qimage.setText("GasClipMaxNorm",  QString::number(GasClipMaxNorm));
    qimage.setText("StarGamma",       QString::number(StarGamma));
    qimage.setText("StarBeta",        QString::number(StarBeta));
    qimage.setText("StarClipMin",     QString::number(StarClipMin));
    qimage.setText("StarClipMaxVal",  QString::number(StarClipMaxVal));
    qimage.setText("StarClipMaxNorm", QString::number(StarClipMaxNorm));
    qimage.setText("DepthOfField",    QString::number(DepthOfField));
    qimage.setText("GSnap Version",   VERSION);
}
