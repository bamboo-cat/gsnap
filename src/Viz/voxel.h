/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief The voxel class, used for volume rendering.
*/

#ifndef VOXEL_H
#define VOXEL_H

#include "Core/global.h"


/*! \brief Manages individual voxel data in the volume rendering scheme.

    This elementary voxel class enables voxels to be added and multiplied
    by a scalar. */
struct voxel
{
private:

    float mass;
    float blue;
    float red;
    float u;
    //float met;

public:

    voxel();
    voxel(const voxel& s);

    void set_mass(float _mass) { mass = _mass; }
    float get_mass() const { return mass; }

    void set_blue(float _blue) { blue = _blue; }
    float get_blue() const { return blue; }

    void set_red(float _red) { red = _red; }
    float get_red() const { return red; }

    void set_u(float _u) { u = _u; }
    float get_u() const { return u; }

    void set_color(const float b, const float r) { blue = b; red = r; }
    void add_color(const float b, const float r) { blue += b; red += r; }

    //void set_met(float _met) { met = _met; }
    //float get_met() const { return met; }

    voxel operator=(const voxel& s);
    voxel operator+(const voxel& s);
    voxel operator+=(const voxel& s);
    friend voxel operator*(const float c, const voxel& s);
    friend voxel operator*(const voxel& s, const float c);

    /*! return a measure of the total gas and star content within this voxel. */
    float sum() const {return mass + blue + red;}

    /*! set all quantities to zero. */
    void zero();
};

#endif // VOXEL_H
