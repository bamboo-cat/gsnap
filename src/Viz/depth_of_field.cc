/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief The implementation of the depth_of_field class.
*/

#include "depth_of_field.h"

#include <cmath>
#include <iostream>

using namespace std;


depth_of_field::depth_of_field()
{
    effect_enabled = false;
    // there is no need to initiaize other values since effect_enabled == false
    // implies that no other members can be accessed.
}


depth_of_field::depth_of_field(float _fractional_depth,
                               pix _total_depth,
                               pix _xres,
                               pix _yres)
{
    init(_fractional_depth,
         _total_depth,
         _xres,
         _yres);
}


void depth_of_field::init(float _fractional_depth,
                          pix _total_depth,
                          pix _xres,
                          pix _yres)
{
    if (_fractional_depth >= 1.0)
    {
        gsnap::print_error("The quantity, fractional_depth should be < 1.0.",
                           err::syntax);
    }

    if (kernels.size() > 0)
    {
        gsnap::print_error("It appears that you have tried to call "
                           "depth_of_field::init() more than once.", err::bug);
    }

    fractional_depth = _fractional_depth;
    total_depth = _total_depth;
    xres = _xres;
    yres = _yres;

    npixels = xres * yres;

    // determine the positions of the focus-blur boundaries:

    near_cut = pix(0.5 * (1.0 - fractional_depth) * total_depth);

    far_cut = total_depth - near_cut;

    // the maximum amount of blurring
    /// \todo Make this a user-defined quantity
    max_blur = 39;

    gradient = float(max_blur) / float (near_cut);

    // practicality check: if the specified settings would only blur a few
    // slices, then the depth of field effect isn't needed.

    effect_enabled = ( near_cut > 50 );

    compute_kernels();
}


int depth_of_field::blur(voxel* voxel_slice, pix k)
{
    if (!effect_enabled) { return 0; }

    if ( (k > near_cut) && (k < far_cut) ) { return 0; }

    // determine the blur-level

    uint blur_level = 0;

    if (k <= near_cut)
    {
        blur_level = max_blur - uint(gradient * k);
    }
    else if (k >= far_cut)
    {
        blur_level = 1 + uint(gradient * (k - far_cut) );
    }

    // perform the blur operation if the slice is not empty (i.e., as soon as it
    // becomes evident that the slice is non-empty)

    float slice_sum = 0;

    for (uint i = 0; i < npixels; i += 4)
    {
        slice_sum += voxel_slice[i].sum();

        if (slice_sum > 1e-6)
        {
            blur_slice(voxel_slice, blur_level);
            return 1;
        }
    }

    return 0;
}


void depth_of_field::compute_kernels()
{
    constexpr float isqrt2 = 1.0 / sqrt(2);

    // The standard deviation of the Gaussian kernel is the "blur radius"
    float blur_radius = 0.4;

    // loop over all kernels

    for (uint i = 0; i < max_blur + 1; ++i)
    {
        // The ith kernel
        kernel krnl;

        // One pixel width in units of the blur radius
        float pixel_width = 1.0 / blur_radius;

        // The radius of the kernel in pixel widths
        pix kernel_radius = ceil(3.0 * blur_radius);

        float prev_sum = 0;

        // compute kernel entries

        for (pix j = 0; j < kernel_radius; ++j)
        {
            float sum = erf(isqrt2 * pixel_width * (0.5 + j) );
            float val = (j > 0) ? 0.5 * (sum - prev_sum) : sum;
            prev_sum = sum;
            krnl.push_back(val);
        }

        // increase blur_radius by a factor of 1.12 (experimentally determined)

        blur_radius *= 1.12;

        // normalize the kernel

        float norm = 0;

        for (int i = 1 - int(kernel_radius); i < int(kernel_radius); ++i)
        {
            norm += krnl[abs(i)];
        }

        float inorm = 1.0 / norm;

        for (float & i : krnl ) { i *= inorm; }

        kernels.push_back(krnl);
    }
}


int depth_of_field::blur_slice(voxel* slice_buf, uint blur_level)
{
    /* Currently, this performs a Gaussian blur operation by blurring the
       voxel plane along its columns then along its rows using a linear kernel.
       This is faster and simpler than using a single pass with a 2D kernel. */

    // The number of entries in (or length of) the kernel signature
    const int kern_len = int(kernels[blur_level].size());

    const float* kern = kernels[blur_level].data();

    // first index of buffer0
    const uint buffer0 = 0;

    // first index of buffer1
    const uint buffer1 = npixels;

    // initialize the buffer identifiers

    uint read_buf = buffer0;
    uint write_buf = buffer1;

    // clear the write buffer (set entries to zero)

    for (uint i = 0; i < npixels; ++i) { slice_buf[write_buf + i].zero(); }

    // blur along the j-axis

    /*! \todo Try to further optimize the following loop. It is the most
         expensive step of the blur operation because it does not use caching
         or prefetching very effectively. There are many non-sequential reads
         and writes because we are blurring columns rather than rows in this
         step. Transposing the array would speed things up, but performing two
         transpose operations is also expensive. */

    // incrementing the i loop by stripe_size columns in the following way
    // allows for a slightly better memory access pattern. The value of
    // stripe-size was determined experimentally.

    const uint stripe_size = 8;

    #pragma omp parallel for schedule(dynamic, 2)

    for (pix i = 0; i < xres - stripe_size; i += stripe_size)
    {
        for (pix j = 0; j < yres; ++j)
        {
            voxel* const read_buffer = &slice_buf[read_buf + xres * j + i];

            // if there is nothing to blur, then continue

            {
                float sum = 0;

                for (uint s = 0; s < stripe_size; ++s)
                {
                    sum += (read_buffer + s)->sum();
                }

                if (sum < 1e-9) { continue; }
            }

            // determine the limits of the for loop

            const int jp_low = int(j) - kern_len;
            const int jp_high = j + kern_len;

            const pix jp_min = (jp_low < -1) ? 0 : jp_low + 1;
            const pix jp_max = (pix(jp_high) > yres ) ? yres : jp_high;

            // The initial kernel index
            int k = int(jp_min) - int(j);

            for (pix jp = jp_min; jp < jp_max; ++jp)
            {
                const float konst = kern[abs(k++)];

                // the remainder may be somewhat difficult to read, but it's
                // easier for the compiler to optimize correctly

                voxel* wb = &slice_buf[write_buf + xres * jp + i];

                const voxel* rb = read_buffer;

                for (uint s = 0; s < stripe_size; ++s)
                {
                    *wb += konst * *rb;
                    ++wb;
                    ++rb;
                }
            }
        }
    }

    read_buf = buffer1;
    write_buf = buffer0;

    // clear the write buffer (set entries to zero)

    for (uint i = 0; i < npixels; ++i) { slice_buf[write_buf + i].zero(); }

    // blur along the i-axis

    #pragma omp parallel for schedule(dynamic, 2)

    for (pix j = 0; j < yres; ++j)
    {
        for (pix i = 0; i < xres; ++i)
        {
            const voxel value = slice_buf[read_buf + xres * j + i];

            // If there is nothing to blur here, continue.
            if (value.sum() < 1e-10) { continue; }

            // determine the limits of the for loop

            const int ip_low = int(i) - kern_len;
            const int ip_high = i + kern_len;

            const pix ip_min = (ip_low < -1) ? 0 : ip_low + 1;
            const pix ip_max = (pix(ip_high) > xres ) ? xres : ip_high;

            // The initial kernel index
            int k = int(ip_min) - int(i);

            voxel* wb = &slice_buf[write_buf + xres * j + ip_min];

            for (pix ip = ip_min; ip < ip_max; ++ip)
            {
                *wb += kern[abs(k++)] * value;
                ++wb;
            }
        }
    }

    return 1;
}
