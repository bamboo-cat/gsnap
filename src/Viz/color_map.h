/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Provides a color map for connecting floating point values with colors
 *         in the output images.
 */


#ifndef COLOR_MAP_H
#define COLOR_MAP_H

#include "Core/global.h"

#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string>
#include <QImage>
#include <QString>
#include <QColor>


/*! \brief Reads a colorbar and maps a single floating point value to an RGB
    triplet.

    This class reads external, user-defined color maps. Colors are mapped from
    a single floating point value to a color, specified in a PNG image file. The
    PNG input map is any 256x1 image.*/
struct color_map
{
    color_map();

    color_map(const float temperature, const float brightness);

    void init();

    void set_value(const float temperature, const float brightness);

    void use_colorbar(std::string filename);

    float get_red() const {return red;}
    float get_green() const {return green;}
    float get_blue() const {return blue;}

protected:

    float red;
    float green;
    float blue;
    rgb<float>  map[256];
    std::string external_map_file;
    bool use_external_map;

    void make_map();
};

#endif // COLOR_MAP_H
