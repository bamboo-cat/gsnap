/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Implements GADGET snapshot file input/output.
 *
 *  \todo Add code for reading and writing multi-file snapshots.
 */

#include "IO/gadget.h"
#include "Core/particles.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <QByteArray>

#pragma GCC optimize ("Os")

using namespace std;


/* Custom extraction and insertion operators are defined here because they
   collide with global definitions in the Qt Framework. There appears to be no
   clean way of avoiding the name collision using namespaces. Further
   investigation is needed if these are to be made available to all of the IO
   routines for all formats. */

fstream& operator>>(fstream& stream, int& x)
{
    stream.read((char*)&x, sizeof(int));
    return stream;
}


fstream& operator>>(fstream& stream, float& x)
{
    stream.read((char*)&x, sizeof(float));
    return stream;
}


fstream& operator<<(fstream& stream, int& x)
{
    stream.write((char*)&x, sizeof(int));
    return stream;
}


fstream& operator<<(fstream& stream, float& x)
{
    stream.write((char*)&x, sizeof(float));
    return stream;
}


template <typename T>
fstream& operator>>(fstream& stream, vector<T> &x)
{
    const uint len = x.size();
    T val;

    for (uint i = 0; i < len; ++i)
    {
        stream.read((char*)&val, sizeof(T));
        x[i] = val;
    }

    return stream;
}


template <typename T>
fstream& operator<<(fstream& stream, vector<T> &x)
{
    uint len = x.size();
    T val;

    for (uint i = 0; i < len; ++i)
    {
        val = x[i];
        stream.write((char*)&val, sizeof(T));
    }

    return stream;
}


snap_format gadget_io::check_format(QString qfname)
{
    QByteArray str = qfname.toLocal8Bit();

    char* c_str = str.data();

    return gadget_io::check_format(c_str);
}


snap_format gadget_io::check_format(char* name)
{
    int blksize, prev_blksize;

    fstream fdpp;
    fdpp.open(name, ios::in | ios::binary);

    if (fdpp.fail())
    {
        string msg = "Unable to open " + string(name);
        gsnap::print_error(msg, err::io);
        //return false;
    }

    fdpp >> blksize;

    if (blksize != 256)
    {
        fdpp.close();
        gsnap::print_warning("The file is not in the correct format.\n");
        return snap_format::none;
    }

    fdpp.seekg(0, ios::beg);

    for (uint i = 0; i < 20; ++i)
    {
        fdpp >> blksize;

        prev_blksize = blksize;

        fdpp.seekg(blksize, ios::cur);

        fdpp >> blksize;

        if (prev_blksize != blksize)
        {
            gsnap::print_warning("The file appears to be corrupted!\n");
            fdpp.close();
            return snap_format::none;
        }

        /* If the 13th block is larger than 1000 bytes, we assume that this is
           a GADGET 2 Snapshot, since block 13 is the potential block.
           If it is smaller than 1000 bytes, we must be reading a GADGET 3
           snapshot (in which case the 13th block stores the black hole mass)
        */
        if (i == 13)
        {
            return (blksize > 1000) ? snap_format::Gadget2
                   : snap_format::Gadget3;
        }
    }

    fdpp.close();
    return snap_format::none;
}


void gadget_io::save_as(QString qfname)
{
    QByteArray str = qfname.toLocal8Bit();

    char* c_str = str.data();

    save_as(c_str);
}


void gadget_io::load(QString qfname)
{
    QByteArray str = qfname.toLocal8Bit();

    char* c_str = str.data();

    load(c_str);
}


void gadget_io::load(char* fname)
{
    if (initialized) { clear_vecs(); }

    if (fname == NULL)
    {
        gsnap::print_error("Invalid file name.", err::syntax);
    }

    file_fmt = check_format(fname);

    if (file_fmt == snap_format::none)
    {
        gsnap::print_error("Invalid file format.", err::io);
    }

    fstream fd;
    fd.open(fname, ios::in | ios::binary);

    int i, type, pc, blksize;
    int ntot_withmasses;

    fd >> blksize;
    fd.read((char*)&header, 256);
    fd >> blksize;

    // get the number of gas particles
    Ngas = header.npart[0];

    // get the number of DM particles
    Ndm = header.npart[1];

    // get the number of disk star particles
    Ndisk = header.npart[2];

    // get the number of bulge star particles
    Nbulge = header.npart[3];

    // get the number of new star particles
    Nnew = header.npart[4];

    // get the number of black hole particles
    Nbh = header.npart[5];

    // determine the total number of particles from the header data
    for (i = 0, Ntot = 0; i < 6; ++i)
        { Ntot += header.npart[i]; }

    // allocate the particle position, velocity, and ID arrays
    vector<float> _pos(3 * Ntot, 0);
    pos = _pos;
    vel = _pos;

    vector<int> _id(Ntot, 0);
    id = _id;

    // allocate an array to store particle masses
    vector<float> _m(Ntot, 0);
    m = _m;

    // if there are gas particles in the snapshot, then allocate
    // vectors to store the gas particle properties.
    if (Ngas)
    {
        vector<float> _sph(Ngas, 0);

        u = rho = ne = nh = hsml = sfr = _sph;
    }

    // determine how many particles have metallicity data.
    int n_with_metalicity = header.npart[0] + header.npart[4];

    // allocate age vector for new star particles.
    vector<float> _age(header.npart[4], 0);
    age = _age;

    // allocate metallicity vector for all stars that have metallicity data.
    vector<float> _metz(n_with_metalicity, 0);
    metz = _metz;

    // allocate the potential vector.
    pot = _m;

    //allocate the black hole vectors
    vector<float> _Mbh(Nbh, 0);
    Mbh = _Mbh;
    Mdot_bh = _Mbh;

    fd >> blksize; // read the positions of particles
    fd >> pos;
    fd >> blksize;

    fd >> blksize; // read the velocities of particles
    fd >> vel;
    fd >> blksize;

    fd >> blksize; // read the particle ID numbers
    fd >> id;
    fd >> blksize;


    // count the number of variable-mass particles
    for (i = 0, ntot_withmasses = 0; i < 6; ++i)
    {
        if (header.mass[i] == 0)
            { ntot_withmasses += header.npart[i]; }
    }

    /* loop over all types of particles, then loop over all particles within
       each type. If the mass of the particle type (in the header) is equal
       to zero,  the masses are variable. Otherwise all 'type' particle
       masses are the same. */
    if (ntot_withmasses) { fd >> blksize; }

    for (type = 0, pc = 0; type < 6; type++)
    {
        for (i = 0; i < header.npart[type]; ++i)
        {
            if (header.mass[type] == 0)
            {
                fd >> m[pc];
            }
            else
            {
                m[pc] = header.mass[type];
            }

            pc++;
        }
    }

    if (ntot_withmasses) { fd >> blksize; }

    // if the snapshot contains gas data,
    if (Ngas)
    {
        fd >> blksize; // read the internal energy block
        fd >> u;
        fd >> blksize;

        fd >> blksize; // read the mass-density block
        fd >> rho;
        fd >> blksize;

        fd >> blksize; // read the electron density block
        fd >> ne;
        fd >> blksize;

        fd >> blksize; // read the neutral hydrogen density block
        fd >> nh;
        fd >> blksize;

        fd >> blksize; // read the SPH smoothing length block
        fd >> hsml;
        fd >> blksize;

        fd >> blksize; // read the star formation rate block
        fd >> sfr;
        fd >> blksize;
    }

    fd >> blksize; // read the stellar creation time / age block
    fd >> age;
    fd >> blksize;

    fd >> blksize; // read the metallicity block
    fd >> metz;
    fd >> blksize;

    if (file_fmt == snap_format::Gadget3)
    {
        fd >> blksize; // read the black hole masses
        fd >> Mbh;
        fd >> blksize;

        fd >> blksize; // read the black hole accretion rate
        fd >> Mdot_bh;
        fd >> blksize;

        fd >> blksize; // read the gravitational potential block
        fd >> pot;
        fd >> blksize;
    }
    else if (file_fmt == snap_format::Gadget2)
    {
        fd >> blksize; // read the gravitational potential block
        fd >> pot;
        fd >> blksize;

        fd >> blksize; // read the black hole masses
        fd >> Mbh;
        fd >> blksize;

        fd >> blksize; // read the black hole accretion rate
        fd >> Mdot_bh;
        fd >> blksize;
    }
    else
    {
        gsnap::print_error("The snapshot format could not not determined.",
                           err::io);
    }

    fd.close();

    // compute the sum of the mass array and add to the total mass of the system

    for (i = 1, Mtot = 0; i <= Ntot; ++i) { Mtot += m[i]; }

    // compute the total mass of the dark matter component

    for (i = 1 + Ngas, Mdm = 0; i <= (Ngas + Ndm); ++i) { Mdm += m[i]; }

    initialized = true;
}


void gadget_io::save_as(char* fname)
{

    int i, type, pc, blklen, ntot_withmasses;

    fstream fd;
    fd.open(fname, ios::out | ios::binary);

    if (!fd)
    {
        string msg = "Could not open the file " + string(fname);
        gsnap::print_error(msg, err::io);
    }

    update_header();

    // write the header

    blklen = sizeof(gadget_header);
    fd << blklen;
    fd.write((char*) &header, sizeof (gadget_header));
    fd << blklen;

    // write the position block

    blklen = 3 * Ntot * sizeof(float);
    fd << blklen;
    fd << pos;
    fd << blklen;

    // write the velocity block

    blklen = 3 * Ntot * sizeof(float);
    fd << blklen;
    fd << vel;
    fd << blklen;

    // write the ID number block

    blklen = Ntot * sizeof(int);
    fd << blklen;
    fd << id;
    fd << blklen;

    for (i = 0, ntot_withmasses = 0; i < 6; ++i)
    {
        if (header.mass[i] == 0)
            { ntot_withmasses += header.npart[i]; }
    }

    // write masses for particles of variable mass. It only makes sense to do
    // this if there are particles of variable mass in the snapshot.

    if (ntot_withmasses)
    {
        blklen = ntot_withmasses * sizeof(float);
        fd << blklen;

        for (type = 0, pc = 0; type < 6; type++)
        {
            for (i = 0; i < header.npart[type]; ++i)
            {
                if (header.mass[type] == 0)
                {
                    fd << m[pc++];
                }
                else
                {
                    ++pc;
                }
            }
        }

        fd << blklen;
    }


    if (header.npart[0] > 0)
    {
        // write the internal energy block

        blklen = header.npart[0] * sizeof(float);
        fd << blklen;
        fd << u;
        fd << blklen;

        //write the density block

        blklen = header.npart[0] * sizeof(float);
        fd << blklen;
        fd << rho;
        fd << blklen;

        // write the electron density block

        blklen = header.npart[0] * sizeof(float);
        fd << blklen;
        fd << ne;
        fd << blklen;

        // write the neutral hydrogen density block

        blklen = header.npart[0] * sizeof(float);
        fd << blklen;
        fd << nh;
        fd << blklen;

        // write the smoothing length block

        blklen = header.npart[0] * sizeof(float);
        fd << blklen;
        fd << hsml;
        fd << blklen;

        // write the star formation rate block

        blklen = header.npart[0] * sizeof(float);
        fd << blklen;
        fd << sfr;
        fd << blklen;
    }

    // write the particle creation time block (for new star particles only)

    blklen = header.npart[4] * sizeof(float);
    fd << blklen;
    fd << age;
    fd << blklen;

    // write the metallicity block for particles that have metallicity data.

    blklen = (header.npart[4] + header.npart[0]) * sizeof(float);
    fd << blklen;
    fd << metz;
    fd << blklen;

    if (file_fmt == snap_format::Gadget2)
    {
        // write the potential energy block.

        blklen = Ntot * sizeof(float);
        fd << blklen;
        fd << pot;
        fd << blklen;

        // write the black hole mass block.

        blklen = Nbh * sizeof(float);
        fd << blklen;
        fd << Mbh;
        fd << blklen;

        // write the accretion rate block.

        blklen = Nbh * sizeof(float);
        fd << blklen;
        fd << Mdot_bh;
        fd << blklen;
    }
    else if (file_fmt == snap_format::Gadget3)
    {
        // write the black hole mass block.

        blklen = Nbh * sizeof(float);
        fd << blklen;
        fd << Mbh;
        fd << blklen;

        // write the accretion rate block.

        blklen = Nbh * sizeof(float);
        fd << blklen;
        fd << Mdot_bh;
        fd << blklen;

        // write the potential energy block.

        blklen = Ntot * sizeof(float);
        fd << blklen;
        fd << pot;
        fd << blklen;
    }
    else
    {
        gsnap::print_warning("You are saving in a format that is neither"
                             "GADGET-3 nor GADGET-2. This may not be what you"
                             "intended to do");
    }

    fd.close();
}


void gadget_io::clear_vecs()
{
    pos.clear();
    vel.clear();
    id.clear();
    m.clear();

    if (Ngas)
    {
        u.clear();
        rho.clear();
        ne.clear();
        nh.clear();
        hsml.clear();
        sfr.clear();
    }

    age.clear();
    metz.clear();
    pot.clear();
    Mbh.clear();
    Mdot_bh.clear();
}


void gadget_io::clear_all()
{
    clear_vecs();

    for (uint i = 0; i < 6; ++i)
    {
        header.mass[i] = 0;
        header.npart[i] = 0;
    }

    Ntot = Ngas = Ndm = Ndisk = Nbulge = Nnew = Nbh = Mtot = Mdm = 0;
}


void gadget_io::update_header()
{
    header.npart[0] = Ngas = u.size();
    header.npart[1] = Ndm;
    header.npart[2] = Ndisk;
    header.npart[3] = Nbulge;
    header.npart[4] = Nnew = age.size();
    header.npart[5] = Nbh = Mbh.size();

    for (int i = 0, Ntot = 0; i < 6; ++i)
    {
        Ntot += header.npart[i];
    }

    // consistency check
    if ( id.size() != uint(Ntot) )
    {
        gsnap::print_error("The number of particles in the snapshot (Ntot) is "
                           "inconsistent with the length of the particle id "
                           "array!", err::io);
    }
}


void gadget_io::gadget_to_snapshot(generic_header& head)
{
    uint ntot = 0;

    for (uint i = 0; i < 6; ++i)
    {
        head.n_category[i] = header.npart[i];
        ntot += head.n_category[i];
    }

    head.n_total = ntot;

    for (uint i = 0; i < 6; ++i) { head.mass[i] = header.mass[i]; }

    head.time = header.time;
    head.redshift = header.redshift;
    head.flag_sfr = header.flag_sfr;
    head.flag_feedback = header.flag_feedback;

    for (uint i = 0; i < 6; ++i) { head.npartTotal[i] = header.npartTotal[i]; }

    head.flag_cooling = header.flag_cooling;
    head.num_files = header.num_files;
    head.BoxSize = header.BoxSize;
    head.Omega0 = header.Omega0;
    head.OmegaLambda = header.OmegaLambda;
    head.HubbleParam = header.HubbleParam;
    head.flag_stellarage = header.flag_stellarage;
    head.flag_metals = header.flag_metals;

    for (uint i = 0; i < 6; ++i)
    {
        head.npartTotalHighWord[i] = header.npartTotalHighWord[i];
    }

    head.flag_entropy_instead_u = header.flag_entropy_instead_u;
}


void gadget_io::snapshot_to_gadget(generic_header& head)
{
    for (uint i = 0; i < 6; ++i) { header.npart[i] = head.n_category[i]; }

    for (uint i = 0; i < 6; ++i) { header.mass[i] = head.mass[i]; }

    header.time = head.time;
    header.redshift = head.redshift;
    header.flag_sfr = head.flag_sfr;
    header.flag_feedback = head.flag_feedback;

    for (uint i = 0; i < 6; ++i) { header.npartTotal[i] = head.npartTotal[i]; }

    header.flag_cooling = head.flag_cooling;
    header.num_files = head.num_files;
    header.BoxSize = head.BoxSize;
    header.Omega0 = head.Omega0;
    header.OmegaLambda = head.OmegaLambda;
    header.HubbleParam = head.HubbleParam;
    header.flag_stellarage = head.flag_stellarage;
    header.flag_metals = head.flag_metals;

    for (uint i = 0; i < 6; ++i)
    {
        header.npartTotalHighWord[i] = head.npartTotalHighWord[i];
    }

    header.flag_entropy_instead_u = head.flag_entropy_instead_u;

    for (uint i = 0; i < 60; ++i) { header.fill[i] = 0; }
}


void gadget_io::gadget_to_snapshot(vmeta_particle& data)
{
    data.clear();
    data.reserve( id.size() );

    // build meta particles
    // lots of data.push_back(); operations
}


void gadget_io::snapshot_to_gadget(vmeta_particle& data)
{
    Ntot = data.size();

    // fill in these quantities:

//    int         Ngas;
//    int         Ndm;
//    int         Ndisk;
//    int         Nbulge;
//    int         Nnew;
//    int         Nbh;
//    float       Mtot;
//    float       Mdm;
//    vfloat      pos;
//    vfloat      vel;
//    vfloat      m;
//    vint        id;
//    vfloat      u;
//    vfloat      rho;
//    vfloat      ne;
//    vfloat      nh;
//    vfloat      hsml;
//    vfloat      sfr;
//    vfloat      age;
//    vfloat      metz;
//    vfloat      pot;
//    vfloat      Mbh;
//    vfloat      Mdot_bh;

    // this may involve several passes over the data vector.

    // black hole accretion needs to be handled in some way...possibly adding
    // another quantity to the meta_particle struct.
}
