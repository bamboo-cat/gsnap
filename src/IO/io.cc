/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Contains general (i.e., format-agnostic) IO routines. */

#include "IO/io.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>

#pragma GCC optimize ("Os")

using namespace std;


void snapshot::open(char* fname)
{
    current_format = deduce_format(fname);

    if (current_format == snap_format::Gadget)
    {
        gadget_io gadget(fname);
        gadget.to_snapshot(header, data);
    }
    else
    {
        gsnap::print_error("The snapshot format was not recognized; the "
                           "snapshot could not be loaded.", err::io);
    }
}


void snapshot::save_as(char* fname, snap_format format)
{
    current_format = format;
    file_name = fname;

    if ( (current_format == snap_format::Gadget)
            || (current_format == snap_format::Gadget2) )
    {
        gadget_io gadget;
        gadget.from_snapshot(header, data);
        gadget.set_file_format(snap_format::Gadget2);
        gadget.save_as(file_name);
    }
    else if (current_format == snap_format::Gadget3)
    {
        gadget_io gadget;
        gadget.from_snapshot(header, data);
        gadget.set_file_format(snap_format::Gadget2);
        gadget.save_as(file_name);
    }
    else if (current_format == snap_format::none)
    {
        gsnap::print_warning("No file format was specified nor implied. The "
                             "file will not be saved.");
    }
}


snap_format snapshot::validate_format(char* fname)
{
    snap_format fmt = deduce_format(fname);

    if (fmt == snap_format::Gadget)
    {
        return gadget_io::check_format(fname);
    }
    else
    {
        return snap_format::none;
    }
}


snap_format snapshot::deduce_format(const char* fname)
{
    fstream fdpp;
    fdpp.open(fname, ios::in | ios::binary);

    if (fdpp.fail())
    {
        string msg = "Unable to open " + string(fname);
        gsnap::print_error(msg, err::io);
    }

    int magic_number;

    fdpp >> magic_number;

    if (magic_number == 256)
    {
        magic_number = 0;

        fdpp.seekg(256, ios::cur);

        fdpp >> magic_number;

        if (magic_number == 256)
        {
            fdpp.close();
            return snap_format::Gadget;
        }
    }

    return snap_format::none;
}
