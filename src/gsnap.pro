# http://pepper.troll.no/s60prereleases/doc/qmake-variable-reference.html
# http://pepper.troll.no/s60prereleases/doc/qmake-manual.html

# Note: this currently assumes that the code will be compiled using GCC (g++)
# There also may be some Debian or Ubuntu-specific installation settings.

TEMPLATE = app
TARGET = gsnap
QT += webkit
QT += script

# specify a release or debug build
CONFIG += release
CONFIG -= exceptions

DEPENDPATH += .
INCLUDEPATH += .
QMAKE_LIBS += -fopenmp

# specific settings for release builds
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast -fopenmp -ffast-math -std=c++11 \
                          -ffunction-sections -fdata-sections    \
                          -fmodulo-sched -msse -fno-exceptions -fno-rtti
                          #-fprofile-generate

QMAKE_CFLAGS_RELEASE   -= -O2
QMAKE_CFLAGS_RELEASE   += -Ofast -fopenmp -ffast-math -ffunction-sections \
                          -fdata-sections -fmodulo-sched
                          #-funroll-loops --param max-unroll-times=16

QMAKE_LFLAGS_RELEASE    = -Ofast -Wl,-gc-sections -fno-exceptions \
                          #-fprofile-generate

# Input
HEADERS   += \
             Core/global.h \
             Core/particle_type.h \
             Core/interpolate.h \
             Core/basic_particle.h \
             Core/particles.h \
             IO/basic_io.h \
             IO/io.h \
             IO/gadget.h \
             Viz/image.h \
             Viz/pixel_array.h \
             Viz/voxel_array.h \
             Viz/color_map.h \
             Viz/depth_of_field.h \
             Viz/voxel.h \
             CLI/cli_actions.h \
             CLI/param_reader.h \
             CLI/script_reader.h \
             GUI/gui_main.h \
             GUI/gui_preview.h \
             GUI/gui_about.h \
             GUI/gui_select.h \
             GUI/gui_help_browser.h \
             settings.h

FORMS     += GUI/UIs/GSnap.ui \
             GUI/UIs/preview.ui \
             GUI/UIs/About.ui \
             GUI/UIs/help_browser.ui

SOURCES   += main.cc \
             Core/global.cc \
             Core/particle_type.cc \
             Core/interpolate.cc \
             Core/basic_particle.cc \
             Core/particles.cc \
             IO/basic_io.cc \
             IO/io.cc \
             IO/gadget.cc \
             Viz/image.cc \
             Viz/pixel_array.cc \
             Viz/voxel_array.cc \
             Viz/color_map.cc \
             Viz/depth_of_field.cc \
             Viz/voxel.cc \
             CLI/cli_actions.cc \
             CLI/param_reader.cc \
             CLI/script_reader.cc \
             GUI/gui_select.cc \
             GUI/gui_preview.cc \
             GUI/gui_main.cc

RESOURCES += GUI/icons.qrc \
             HTML/help.qrc

# The following is specific to machines with UNIX-like file systems

htmldocs.path = /usr/local/share/doc/gsnap/
htmldocs.files = HTML/html/*

target.path = /usr/local/bin/

scripts.path = /usr/local/share/doc/gsnap/scripts/
scripts.files = python/*.py

# this may be specific to Debian linux systems or GNU/Linux systems
bash.path = /etc/bash_completion.d/
bash.files = bash_autocomplete/gsnap

INSTALLS += htmldocs scripts bash target

