/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Member functions for the parameter file reader class.*/

#include "CLI/param_reader.h"
#include "CLI/cli_actions.h"


#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <QString>
#include <QStringList>
#include <QImage>

#pragma GCC optimize ("Os")

using namespace std;


void param_reader::parse()
{
    if ( gsnap::is_PNG(parameter_file) )
    {
        parse_PNG_paramfile();
    }
    else
    {
        parse_text_paramfile();
    }
}  // param_reader::parse()


void param_reader::parse_PNG_paramfile()
{
    QImage qimage(parameter_file);

    QStringList lines = qimage.text().split("\n", QString::SkipEmptyParts);

    if (lines.length() < 2)
    {
        gsnap::print_error("The parameter file (PNG image, in this case) does "
                           "not contain any parameters. Note that image editing "
                           "software typically removes metadata from PNG images."
                           " This may be the cause of the problem.", err::io);
    }

    for (int i = 0; i < lines.length(); ++i)
    {
        if ( !lines[i].contains("Version") && lines[i].at(0).isUpper() )
        {
            parse_parameter_line(lines[i].toLocal8Bit(), i + 1);
        }
    }
} // param_reader::parse_PNG_paramfile()


void param_reader::parse_text_paramfile()
{
    ifstream parameters ( parameter_file, ifstream::in );

    if (!parameters)
    {
        string msg = "unable to open " + string(parameter_file) + "\n";
        gsnap::print_error(msg, err::io);
    }

    while (parameters.good())
    {
        char line[256];

        static uint line_number = 0;

        ++line_number;

        parameters.getline(line, 256);

        parse_parameter_line(line, line_number);
    }

    parameters.close();
} // param_reader::parse_text_paramfile()


void param_reader::parse_parameter_line(const char* line, const uint line_number)
{
    QString str(line);

    int comment_idx = str.indexOf('#'); // index of the comment character.

    // remove comments from the string

    if (str.contains("#")) { str.chop( str.size() - comment_idx ); }

    // The string will be broken into space-delimited words, so replace all
    // possible delimiters with spaces.

    str.replace(QString("\t"), QString(" "));
    str.replace(QString("="), QString(" "));
    str.replace(QString(":"), QString(" "));

    if ( str.contains(";") )
    {
        // recursively split lines into semicolon delimited sub-lines.

        QStringList sub_str = str.split(";", QString::SkipEmptyParts);

        for (int s = 0; s < sub_str.size(); ++s)
        {
            parse_parameter_line(sub_str[s].toStdString().c_str(), line_number);
        }
    }
    else
    {
        QStringList list = str.split(" ", QString::SkipEmptyParts);

        if (list.size() == 2)
        {
            interpret_parameter_line(list);
        }
        else if ( (list.size() == 1) || (list.size() > 2) )
        {
            string msg = "I don't understand the parameter file line # ";
            msg += QString::number(line_number).toStdString() + ":\n\n\"";
            msg += list.join(" ").toStdString() + "\"\n";
            gsnap::print_error(msg, err::syntax);
        }
    }
} // param_reader::parse_parameter_line()


void param_reader::interpret_parameter_line(const QStringList& line)
{
    QString key = line[0], v = line[1];

    string msg;

    bool ok;

    switch ( param_map[key.toStdString()] )
    {
    case param::ImageWidth:      cli->img.ImageWidth      = v.toUInt(&ok);  break;
    case param::ImageHeight:     cli->img.ImageHeight     = v.toUInt(&ok);  break;
    case param::ViewWidth:       cli->img.ViewWidth       = v.toFloat(&ok); break;
    case param::ViewDepth:       cli->img.ViewDepth       = v.toFloat(&ok); break;
    case param::MinNeighbors:    cli->img.MinNeighbors    = v.toUInt(&ok);  break;
    case param::MaxNeighbors:    cli->img.MaxNeighbors    = v.toUInt(&ok);  break;
    case param::GasOpacity:      cli->img.GasOpacity      = v.toFloat(&ok); break;
    case param::GasGamma:        cli->img.GasGamma        = v.toFloat(&ok); break;
    case param::GasBeta:         cli->img.GasBeta         = v.toFloat(&ok); break;
    case param::GasClipMin:      cli->img.GasClipMin      = v.toFloat(&ok); break;
    case param::GasClipMaxVal:   cli->img.GasClipMaxVal   = v.toFloat(&ok); break;
    case param::GasClipMaxNorm:  cli->img.GasClipMaxNorm  = v.toFloat(&ok); break;
    case param::StarGamma:       cli->img.StarGamma       = v.toFloat(&ok); break;
    case param::StarBeta:        cli->img.StarBeta        = v.toFloat(&ok); break;
    case param::StarClipMin:     cli->img.StarClipMin     = v.toFloat(&ok); break;
    case param::StarClipMaxVal:  cli->img.StarClipMaxVal  = v.toFloat(&ok); break;
    case param::StarClipMaxNorm: cli->img.StarClipMaxNorm = v.toFloat(&ok); break;
    case param::DepthOfField:    cli->img.DepthOfField    = v.toFloat(&ok); break;

    case param::SlitWidth:       cli->slit_width    = v.toFloat(&ok); break;
    case param::SlitLength:      cli->slit_length   = v.toFloat(&ok); break;
    case param::Projections:     cli->N_projections = v.toUInt(&ok);  break;

    case param::ColorMap:

        if ( gsnap::is_PNG( v.toStdString().c_str() ) )
        {
            cli->img.ColorMap = v;
            ok = true;
        }
        else { gsnap::print_error("The ColorMap must be a PNG image.", err::io); }

        break;

    case param::error:

        msg = "I don't recognize \"" + key.toStdString() + "\"\n";
        gsnap::print_error(msg, err::syntax);
    }

    if (!ok)
    {
        msg = "the parameter file value \"" + v.toStdString() + "\" was not "
              "successfully converted to a numerical type.\n";
        gsnap::print_error(msg, err::syntax);
    }
} // param_reader::interpret_parameter_line(


void param_reader::make_param_map()
{
    param_map["ImageWidth"     ] = param::ImageWidth;
    param_map["ImageHeight"    ] = param::ImageHeight;
    param_map["ViewWidth"      ] = param::ViewWidth;
    param_map["ViewDepth"      ] = param::ViewDepth;
    param_map["SlitWidth"      ] = param::SlitWidth;
    param_map["SlitLength"     ] = param::SlitLength;
    param_map["Projections"    ] = param::Projections;
    param_map["MinNeighbors"   ] = param::MinNeighbors;
    param_map["MaxNeighbors"   ] = param::MaxNeighbors;
    param_map["GasOpacity"     ] = param::GasOpacity;
    param_map["GasGamma"       ] = param::GasGamma;
    param_map["GasBeta"        ] = param::GasBeta;
    param_map["GasClipMin"     ] = param::GasClipMin;
    param_map["GasClipMaxVal"  ] = param::GasClipMaxVal;
    param_map["GasClipMaxNorm" ] = param::GasClipMaxNorm;
    param_map["StarGamma"      ] = param::StarGamma;
    param_map["StarBeta"       ] = param::StarBeta;
    param_map["StarClipMin"    ] = param::StarClipMin;
    param_map["StarClipMaxVal" ] = param::StarClipMaxVal;
    param_map["StarClipMaxNorm"] = param::StarClipMaxNorm;
    param_map["DepthOfField"   ] = param::DepthOfField;
    param_map["ColorMap"       ] = param::ColorMap;
} // param_reader::make_param_map()
