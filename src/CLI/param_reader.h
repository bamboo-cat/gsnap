/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief The parameter file reader class.*/

#ifndef PARAM_READER_H
#define PARAM_READER_H

#include "Core/global.h"
#include "Core/particles.h"
#include "Viz/image.h"

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <QString>
#include <QStringList>

class cli_actions;


/*!
 * \brief Reads and interprets parameter files.
 *
 * The param_reader class reads and interprets input parameters specified in the
 * form of text files, PNG images, and command line strings. The class updates
 * an object of class cli_actions with the appropriate values.
 *
 * Parameter files consist of parameter-value pairs. Each parameter is a
 * CamelCase keyword which is mapped to an internal variable. In a text
 * parameter file, the parameter name and value must appear on the same line,
 * separated by only whitespace, equals signs, or colons. Multiple parameters
 * can be included in one line by using a semicolon to separate pairs. For
 * example, this is a valid parameter file:
 *
 \code{.py}

  # this is a comment

  # general image creation properties
  ImageWidth    1280 # measured in pixels
  ImageHeight   720 # measured in pixels
  ViewWidth    190  # measured in kiloparsecs

  ViewDepth:300  # measured in kiloparsecs

  GasGamma=0.55

  StarGamma 0.4; StarClipMin=0

 \endcode
 *
 * However, note that parameter lines are limited to 256 characters.
 *
 * When GSnap saves a PNG image, the parameters that were used to create the
 * image are stored in the image's metadata. This allows the user to use the
 * "recipe" from a previous output image to create a new image. The parameters
 * can also be extracted using the command line tool, pngmeta
 * (http://www.libpng.org/pub/png/apps/pngmeta.html), as well as some image
 * editing and organization software packages.
 *
 * When the `--param` command line flag is used, the string following the flag
 * is interpreted as the contents of a parameter file. For example:
 *
 * \verbatim
   gsnap --view gas --param "ImageWidth=1280; ImageHeight=720" snapshot_010
   \endverbatim
 */
class param_reader
{
protected:

    char* parameter_file;
    cli_actions* cli;

    /*  The entries in this enumerations are used to map parameter file keyword
        strings to integers so that the switch statement can be used when
        parsing the parameter file. */
    enum class param
    {
        error,
        ImageWidth,
        ImageHeight,
        ViewWidth,
        ViewDepth,
        SlitWidth,
        SlitLength,
        Projections,
        MinNeighbors,
        MaxNeighbors,
        GasOpacity,
        GasGamma,
        GasBeta,
        GasClipMin,
        GasClipMaxVal,
        GasClipMaxNorm,
        StarGamma,
        StarBeta,
        StarClipMin,
        StarClipMaxVal,
        StarClipMaxNorm,
        DepthOfField,
        ColorMap
    };

    //  An associative array for connecting parameter file keywords with
    //  elements of the parameters enumeration.
    std::map< std::string, param > param_map;

public:

    param_reader(cli_actions* _cli) :  cli(_cli) { make_param_map(); }

    param_reader(char* parfile, cli_actions* _cli)
        : parameter_file(parfile), cli(_cli)
    {
        make_param_map();
        parse();
    }

    void parse_file(char* parfile)
    {
        parameter_file = parfile;
        parse();
    }

    /*! Parses a parameter string (one line the parameter file). Parameter-value
        pairs can be connected using an equals sign, as in Parameter=value.
        Multiple pairs care separated using semicolons. So,
        "ParameterOne=value; ParameterTwo=value" is valid. The "=" can also be
        omitted; there only needs to be a space between the parameter and the
        value. */
    void parse_line(const char* line)
    {
        parse_parameter_line(line);
    }

protected:

    /*! \brief Parses any parameter file.

        Examines the parameter file to determine which type of file has been
        provided. Currently, parameter files can be written as plain text files
        and PNG image files. Once the type of parameter file has been
        determined, the appropriate parser is called. */
    void parse();

    /*! Sets up an associative array mapping parameter file entries (strings)
        to the parameters enumeration entries. */
    void make_param_map();

    /*! Parses a text parameter file. */
    void parse_text_paramfile();

    /*! Parses parameters stored in the meta data of a PNG image file. */
    void parse_PNG_paramfile();

    /*! Parses a parameter string (one line the parameter file) */
    void parse_parameter_line(const char* line, const uint line_number = 0);

    /*! Interprets parameter file keyword-value pairs. */
    void interpret_parameter_line(const QStringList& line);
};

#endif // PARAM_READER_H
