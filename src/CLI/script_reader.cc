/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Member function definitions for the script_reader class.
 */

#include "CLI/script_reader.h"

#include <QFile>
#include <QTextStream>

#pragma GCC optimize ("Os")

using namespace std;


typedef QScriptValue QSValue;

typedef QScriptEngine QSEngine;

typedef QScriptContext QSContext;

// static members

gadget_io* script_reader::gal = nullptr;

particle_type_group* script_reader::particles = nullptr;

types script_reader::my_types;


script_reader::script_reader(types _my_types,
                             const char* _script_file)
{
    script_filename = QString(_script_file);

    my_types = _my_types;

    setup_engine();

    parse_script();
}


script_reader::~script_reader()
{
    if (gal != nullptr) { delete gal; }

    if (particles != nullptr) { delete particles; }
}


void script_reader::parse_script()
{
    QFile script_file(script_filename);

    if (!script_file.open(QIODevice::ReadOnly))
    {
        gsnap::print_error("Cannot open script file.", err::io);
    }

    QTextStream stream(&script_file);
    QString script = stream.readAll();
    script_file.close();

    QScriptSyntaxCheckResult syntax_check =  QSEngine::checkSyntax(script);

    if (syntax_check.state() == QScriptSyntaxCheckResult::Valid)
    {
        QString result = engine.evaluate(script, script_filename).toString();

        if (result != "undefined") { qDebug() << result; }
    }
    else if (syntax_check.state() == QScriptSyntaxCheckResult::Error)
    {
        int col_num  = syntax_check.errorColumnNumber();
        int line_num = syntax_check.errorLineNumber();
        QString  message = syntax_check.errorMessage();

        qDebug() << "Error on line " << line_num << " column " << col_num;
        qDebug() << message;
    }
    else
    {
        qDebug("Your script is incomplete.");
        qDebug() << syntax_check.errorMessage();
        gsnap::print_error("Fix your script and try again.", err::syntax);
    }
}


void script_reader::setup_engine()
{
    QSValue sv_version = engine.newFunction(gsnap_version);
    engine.globalObject().setProperty("version", sv_version);

    QSValue sv_types = engine.newFunction(particle_types);
    engine.globalObject().setProperty("particle_types", sv_types);

    QSValue sv_load = engine.newFunction(load_snapshot);
    engine.globalObject().setProperty("load_snapshot", sv_load);

    QSValue sv_time = engine.newFunction(snapshot_time);
    engine.globalObject().setProperty("snapshot_time", sv_time);

    QSValue sv_sigma = engine.newFunction(sigma);
    engine.globalObject().setProperty("sigma", sv_sigma);

    QSValue sv_slit = engine.newFunction(set_slit);
    engine.globalObject().setProperty("set_slit", sv_slit);

    QSValue sv_center_on = engine.newFunction(center_on);
    engine.globalObject().setProperty("center_on", sv_center_on);

    QSValue sv_rotate_to = engine.newFunction(rotate_to);
    engine.globalObject().setProperty("rotate_to", sv_rotate_to);

    QSValue sv_set_age_bin = engine.newFunction(set_age_bin);
    engine.globalObject().setProperty("set_age_bin", sv_set_age_bin);

    QSValue sv_close_snapshot = engine.newFunction(close_snapshot);
    engine.globalObject().setProperty("close_snapshot", sv_close_snapshot);

    engine.globalObject().setProperty("Gas", 2);
    engine.globalObject().setProperty("DarkMatter", 4);
    engine.globalObject().setProperty("DiskStar", 8);
    engine.globalObject().setProperty("BulgeStar", 16);
    engine.globalObject().setProperty("NewStar", 32);
    engine.globalObject().setProperty("BlackHole", 64);
}


void script_reader::particles_error()
{
    string msg = "The particles have not been loaded. Use load_particles()\n";
    gsnap::print_error(msg, err::syntax);
} //  script_reader::particles_error()


void script_reader::check_numerical_arguments(QSContext *_context, uint nargs)
{
    for (uint i = 0; i < nargs; ++i)
    {
        if ( !_context->argument(i).isNumber() )
        {
            QStringList btrace = _context->backtrace();

            QString line_number = btrace.back().split(":").back();

            qDebug() << "At line number " + line_number + ":";

            gsnap::print_error("set_slit() expects "
                               + QString::number(nargs).toAscii()
                               + " numerical arguments.",
                               err::syntax);
        }
    }
}


QSValue script_reader::load_snapshot(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)

    if (gal != nullptr)
    {
        delete gal;
        gal = nullptr;
    }

    gal = new gadget_io;

    gal->load(_context->argument(0).toString());

    if (particles != nullptr)
    {
        delete particles;
        particles = nullptr;
    }

    particles = new particle_type_group;

    particles->init(my_types, gal);

    return QSValue(true);
}


QSValue script_reader::particle_types(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)

    my_types.gas   = my_types.dm   = my_types.bh    = 0;
    my_types.bulge = my_types.disk = my_types.stars = 0;

    if (_context->argument(0).isNumber())
    {
        const int tps = _context->argument(0).toInteger();

        if (tps & 2)  { my_types.gas = 1; }

        if (tps & 4)  { my_types.dm = 1; }

        if (tps & 8)  { my_types.disk = 1; }

        if (tps & 16) { my_types.bulge = 1; }

        if (tps & 32) { my_types.stars = 1; }

        if (tps & 64) { my_types.bh = 1; }
    }
    else if (_context->argument(0).isString())
    {
        QString type_string = _context->argument(0).toString();

        auto iter = type_string.begin();

        uint type_id = 0;

        while (iter < type_string.end() &&  type_id < 6)
        {
            switch (type_id)
            {

            case 0: if (*iter == '1') { my_types.gas = 1; }   break;

            case 1: if (*iter == '1') { my_types.dm = 1; }    break;

            case 2: if (*iter == '1') { my_types.disk = 1; }  break;

            case 3: if (*iter == '1') { my_types.bulge = 1; } break;

            case 4: if (*iter == '1') { my_types.stars = 1; } break;

            case 5: if (*iter == '1') { my_types.bh = 1; }    break;
            }

            ++type_id;
            ++iter;
        }
    }
    else
    {
        gsnap::print_error("particle_types() expects an integer or string "
                           "argument", err::syntax);
    }

    return QSValue(0);
}


QSValue script_reader::snapshot_time(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_context)
    Q_UNUSED(_engine)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling snapshot_time()", err::syntax);
    }

    return QSValue( float(gal->header.time) );
}


QSValue script_reader::gsnap_version(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_context)
    Q_UNUSED(_engine)

    return QSValue(QString(VERSION));
}


QSValue script_reader::set_slit(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling set_slit()", err::syntax);
    }

    check_numerical_arguments(_context, 3);

    float width = _context->argument(0).toNumber();
    float length = _context->argument(1).toNumber();
    float alpha = _context->argument(2).toNumber();

    particles->set_slit(width, length, alpha);

    return QSValue(0);
}


QSValue script_reader::sigma(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_context)
    Q_UNUSED(_engine)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling sigma()", err::syntax);
    }

    particles->set_direction(0,0); // this is needed when using sigma_fast().
                                   // Without this, sigma would be measured
                                   // along the NEW theta, phi direction.

    return QSValue( float(particles->sigma_fast()) );
}


QSValue script_reader::rotate_to(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling rotate_to()", err::syntax);
    }

    check_numerical_arguments(_context, 2);

    rad theta = _context->argument(0).toNumber();
    rad phi = _context->argument(1).toNumber();

    particles->rotate(theta, phi);

    return QSValue(0);
}


QSValue script_reader::center_on(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling center_on()", err::syntax);
    }

    check_numerical_arguments(_context, 3);

    kpc x = _context->argument(0).toNumber();
    kpc y = _context->argument(1).toNumber();
    kpc z = _context->argument(2).toNumber();

    float center[] = {0, x, y, z, 0, 0, 0};

    particles->shift(center);
    return QSValue(0);
}


QSValue script_reader::set_age_bin(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling set_age_bin()", err::syntax);
    }

    check_numerical_arguments(_context, 2);

    float min_age = _context->argument(0).toNumber();
    float max_age = _context->argument(1).toNumber();

    float age_bin[] = {min_age, max_age};

    particles->set_age_bin(age_bin);

    return QSValue(0);
}


QSValue script_reader::close_snapshot(QSContext* _context, QSEngine* _engine)
{
    Q_UNUSED(_engine)
    Q_UNUSED(_context)

    if (gal == nullptr)
    {
        gsnap::print_error("Load a snapshot file using load_snapshot() before "
                           "calling close_snapshot()", err::syntax);
    }

    gal->clear_all();

    particles->free();

    return QSValue(0);
}
