"""
This script uses GSnap's fast snapshot preview method (--view option) to create
images of the stars (particle types 2,3, and 4) in all snapshot files in a 
directory.

"""
import glob
#import shutil
#import sys
from subprocess import call

files=glob.glob('snapshot_*')

for i in range(1, files.__len__()):
    command=["./gsnap", "--view","001110", files[i]]
    call(command)
    
command=["./gsnap", "--view","001110", "snapshot_00106"]
call(command)

command=["./gsnap", "--view","001110", "snapshot_00822"]
call(command)
