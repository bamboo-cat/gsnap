#! /bin/bash

#usage: ./animate.sh base_name digits image_type movie_name

#example ./animate.sh img_ 5 jpg animate

base_name=$1
digits=$2
image_type=$3
movie_name=$4
: ${base_name:="snapshot_"}
: ${digits:=5}
: ${image_type:="png"}
: ${movie_name:="animationHD"}
bit_rate='100000k'
frame_rate=25

echo $base_name
echo $image_type
echo $movie_name

mkdir cropped
cp *.$image_type cropped/
echo 'cropping frames...'
#mogrify -crop '1280x720+0+280' cropped/*.$image_type
mogrify -level 0%,70%,1.1 -selective-blur 1x1+6% cropped/*.$image_type
echo "creating movie file, $movie_name.avi"
ffmpeg -i cropped/$base_name'%0'$digits'd.'$image_type -r $frame_rate -b:v $bit_rate  $movie_name.avi
#ffmpeg -i cropped/$base_name'%0'$digits'd.'$image_type -r $frame_rate -vcodec huffyuv -an $movie_name.avi
echo 'deleting temporary files'
rm -r cropped
echo 'Done!'
