"""
This script uses GSnap's SPH rendering scheme to create images of all snapshot
files in a directory.
"""
import glob
from subprocess import call

files=glob.glob('snapshot_*')

for i in range(1, files.__len__()):
    command=["./gsnap", "-g", files[i]]
    call(command)
    
command=["./gsnap", "-g", "snapshot_00106"]
call(command)

command=["./gsnap", "-g", "snapshot_00822"]
call(command)
