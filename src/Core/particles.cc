/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Member functions for the particles class.
*/

#include "Viz/pixel_array.h"
#include "Viz/voxel_array.h"
#include "Core/particles.h"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <ctime>
#include <omp.h>

using namespace std;


particle_type_group::particle_type_group(const types my_types, gadget_io* _gal)
{
    init(my_types, _gal);
}


void particle_type_group::compute_cm(float* cm)
{
#define CM_ARRAY_SIZE 7

    float cm_partial_values[CM_ARRAY_SIZE];

    for (uint i = 0; i < CM_ARRAY_SIZE; ++i) { cm[i] = 0; }

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);

        partic[t].compute_cm(cm_partial_values);

        const float mi = cm_partial_values[0];

        cm[0] += mi;

        for (uint j = 1; j < CM_ARRAY_SIZE; ++j)
        {
            cm[j] += cm_partial_values[j] * mi;
        }
    }

    const float mi_inv = 1.0 / cm[0];

    for (uint j = 1; j < CM_ARRAY_SIZE; ++j)
    {
        cm[j] *= mi_inv;
    }

} // particle_type_group::compute_cm()


bool particle_type_group::find_bh(const uint which_bh, float* cm)
{
    if (my_types.bh)
    {
        return partic[uint(pcat::BlackHole)].find_bh(which_bh, cm);
    }
    else
    {
        return false;
    }
} // particle_type_group::find_bh(


void particle_type_group::find_bh_separation()
{
    if (gal->Nbh < 2)
    {
        BH_separation = 0;
    }
    else
    {
        float p0[] = {0, 0, 0, 0, 0, 0, 0};
        float p1[] = {0, 0, 0, 0, 0, 0, 0};
        kpc dx, dy, dz;

        find_bh(0, p0);
        find_bh(1, p1);
        dx = p0[1] - p1[1];
        dy = p0[2] - p1[2];
        dz = p0[3] - p1[3];

        BH_separation = sqrt(dx * dx  +  dy * dy  +  dz * dz);
    }
} // particle_type_group::find_bh_separation()


void particle_type_group::find_bh_rel_speed()
{
    if (gal->Nbh < 2)
    {
        BH_rel_speed = 0;
    }
    else
    {
        float p0[] = {0, 0, 0, 0, 0, 0, 0};
        float p1[] = {0, 0, 0, 0, 0, 0, 0};
        float dx, dy, dz, dvx, dvy, dvz;

        find_bh(0, p0);
        find_bh(1, p1);
        dx = p0[1] - p1[1];
        dy = p0[2] - p1[2];
        dz = p0[3] - p1[3];

        const float norm = 1.0 / sqrt(dx * dx  +  dy * dy  +  dz * dz);

        dx *= norm;
        dy *= norm;
        dz *= norm;

        dvx = (p0[4] - p1[4]) * dx;
        dvy = (p0[5] - p1[5]) * dy;
        dvz = (p0[6] - p1[6]) * dz;

        BH_rel_speed = abs(dvx + dvy + dvz);
    }
} // particle_type_group::find_bh_rel_speed()


void particle_type_group::collide_on_z()
{
    if (gal->Nbh == 2)
    {
        float p0[] = {0, 0, 0, 0, 0, 0, 0};
        float p1[] = {0, 0, 0, 0, 0, 0, 0};

        find_bh(0, p0);
        find_bh(1, p1);

        shift(p0);

        find_bh(1, p1);

        const kpc r = sqrt(p1[1] *  p1[1] + p1[2] * p1[2] + p1[3] * p1[3]);

        const rad new_theta = acos(p1[3] / r);

        const rad new_phi = atan2(p1[2], p1[1]);

        rotate(new_theta, new_phi);

        set_direction(0, 0);

        set_slit(9, 9, 0);

        find_bh(1, p1);
    }
} // particle_type_group::collide_on_z()


float particle_type_group::get_bh_rel_speed() const
{
    return BH_rel_speed;
} // particle_type_group::get_bh_rel_speed()


void particle_type_group::shift(const float* cm)
{
    uint t;

    for (uint i = 0; i < number_of_types; ++i)
    {
        t = uint(which_types[i]);

        partic[t].shift(cm);
    }

    if (using_rect_mesh)
    {
        update_mas_pos();
    }

} // particle_type_group::shift


void particle_type_group::rotate(const rad _theta, const rad _phi)
{
    theta = _theta;
    phi = _phi;

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);
        partic[t].rotate(theta, phi);
    }

    if (using_rect_mesh)
    {
        update_mas_pos();
    }

} // particle_type_group::rotate()


void particle_type_group::update_gal()
{
    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);

        partic[t].update_gal();
    }

} // particle_type_group::update_gal()


void particle_type_group::renew_snapshot()
{
    if (number_of_types != 6)
    {
        gsnap::print_error("In order to modify a snapshot using "
                           "particle_type_group::renew_snapshot(), all particle"
                           " types must be included.", err::bug);
    }

    gal->clear_all();

    // update the snapshot vectors

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);
        partic[t].renew_snapshot();
    }

    // update the snapshot header and scalars

    gal->Ntot   = n_particles;
    gal->Ngas   = partic[uint(pcat::Gas)].get_n_snap();
    gal->Ndm    = partic[uint(pcat::DarkMatter)].get_n_snap();
    gal->Ndisk  = partic[uint(pcat::DiskStar)].get_n_snap();
    gal->Nbulge = partic[uint(pcat::BulgeStar)].get_n_snap();
    gal->Nnew   = partic[uint(pcat::NewStar)].get_n_snap();
    gal->Nbh    = partic[uint(pcat::BlackHole)].get_n_snap();

    float cm[7];
    compute_cm(cm);
    gal->Mtot   = cm[0];

    partic[uint(pcat::DarkMatter)].compute_cm(cm);
    gal->Mdm    = cm[0];

    for (uint i = 0; i < 6; ++i)
    {
        gal->header.mass[i] = 0;
        gal->header.npart[i] = partic[i].get_n_snap();
    }

    // gal->header.time and other quantities could also be adjusted here.

    // black hole accretion rates should be updated as well! For now, we simply
    // set the values to zero.

for (auto m : gal->Mbh) { gal->Mdot_bh.push_back(m * 0.0); }

} // particle_type_group::renew_snapshot()


void particle_type_group::set_slit(const kpc _width,
                                   const kpc _length,
                                   const rad _alpha)
{
    width = _width;
    length = _length;
    alpha = _alpha;

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);

        partic[t].set_slit(width, length, alpha);
    }

} // particle_type_group::set_slit()


float particle_type_group::sigma()
{
    string msg;

    if ( (width == 0) || (length == 0) )
    {
        msg = "You have not specified the slit dimensions.\n"
              "Use particle_type_group::set_slit(width, length, alpha).";
        gsnap::print_error(msg, err::bug);
    }

    number_in_slit = 0;

    float* los_vel = new float [2 * n_particles]; // consider using vector.

    uint  pos = 0;
    float mass = 0;

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);

        pos = partic[t].v_los(pos, los_vel);

        mass += partic[t].get_slit_mass();

        number_in_slit += partic[t].get_number_in_slit();
    }

    float mv = 0, mv2 = 0, lvi, temp;

    if (mass != 0)
    {
        for (uint i = 0; i < pos; ++i)
        {
            lvi = los_vel[2 * i];

            temp = lvi * los_vel[2 * i + 1];

            mv += temp;

            mv2 += temp * lvi;
        }

        mv /= mass;
        mv2 /= mass;
    }

    delete [] los_vel;

    float variance = mv2 - mv * mv;

    if (variance >= 0)
    {
        return sqrt(variance);
    }
    else // the mass-weighted velocity dispersion is technically undefined here.
    {
        return 0;
    }

} // particle_type_group::sigma()


float particle_type_group::sigma_fast()
{
    if ( (1000 * width <= 0) || (1000 * length <= 0) )
    {
        string msg = "You have not specified the slit dimensions.\n"
                     "Use particle_type::set_slit(width, length, alpha).";
        gsnap::print_error(msg, err::bug);
    }

/////////////////////////////////
    static uint n_age = 0;
    static bool used = false;
//////////////////////////////////


    float* los_vel = new float [2 * n_particles];

    const float ct = cos(theta), st = sin(theta);

    const float cp = cos(phi),   sp = sin(phi);

    const float ca = cos(alpha), sa = sin(alpha);

    const float x_min = -0.5 * width, x_max = 0.5 * width;

    const float y_min = -0.5 * length, y_max = 0.5 * length;

    kpc  x, y, z, x_new, y_new;

    float vx, vz, m, vz_new, age, mass = 0;

    float variance;

    uint t, pos = 0, n_snap;

    bool in_x, in_y, in_age = true;

    for (uint i = 0; i < number_of_types; ++i)
    {
        t = uint(which_types[i]);

        n_snap = partic[t].get_n_snap();

        meta_particle* snap = partic[t].get_snap_pointer();

        for (uint j = 0; j < n_snap; ++j)
        {
            // rotate by phi around the z-axis

            x = cp * snap[j].r[0] + sp * snap[j].r[1];
            y = -sp * snap[j].r[0] + cp * snap[j].r[1];
            z = snap[j].r[2];
            age = snap[j].age;

            vx = cp * snap[j].v[0] + sp * snap[j].v[1];
            vz = snap[j].v[2];

            m = snap[j].m;

            // rotate by theta around the new y-axis

            x_new = ct * x - st * z;

            vz_new = st * vx + ct * vz;

            x = x_new;

            // rotate by -alpha around the new z-axis

            x_new = ca * x + sa * y;
            y_new = -sa * x + ca * y;

            // identify which particles appear in the slit
            in_x = ( (x_new > x_min) && (x_new < x_max) );
            in_y = ( (y_new > y_min) && (y_new < y_max) );

            // identify which particles are in the desired age range

            if (age_cut == 0)
            {
                in_age = true;
            }
            else
            {
                if (age_cut > 0)
                {
                    in_age = (age > age_cut);
                }
                else
                {
                    if (age_cut < 0)
                    {
                        in_age = (age < -age_cut);
                    }
                }
            }

            if ( (age_bin[1] - age_bin[0]) > 1e-5 )
            {
                in_age = ( (age > age_bin[0]) && (age < age_bin[1]) );
            }

            if (!used && in_age)
            {
                ++n_age;
            }

            if (in_x && in_y && in_age)
            {
                los_vel[2 * pos] = vz_new;
                los_vel[2 * pos + 1] = m;
                mass += m;
                ++pos;
            }
        }
    }

    float mv = 0, mv2 = 0, lvi, temp;

    if (mass > 0)
    {
        for (uint i = 0; i < pos; ++i)
        {
            lvi = los_vel[2 * i];

            temp = lvi * los_vel[2 * i + 1];

            mv += temp;

            mv2 += temp * lvi;
        }

        mv /= mass;
        mv2 /= mass;
    }

    delete [] los_vel;

    variance = mv2 - mv * mv;

    if (variance >= 0)
    {
        return sqrt(variance);
    }
    else // the mass-weighted velocity dispersion is technically undefined here.
    {
        return 0;
    }

} // particle_type_group::sigma_fast()


void particle_type_group::losvd()
{
    if ( (1000 * width <= 0) || (1000 * length <= 0) )
    {
        string msg = "You have not specified the slit dimensions.\n"
                     "Use particle_type::set_slit(width, length, alpha).";
        gsnap::print_error(msg, err::bug);
    }

    QVector<float> weighted_los_vel;

    const float ct = cos(theta), st = sin(theta);

    const float cp = cos(phi),   sp = sin(phi);

    const float ca = cos(alpha), sa = sin(alpha);

    const float x_min = -0.5 * width, x_max = 0.5 * width;

    const float y_min = -0.5 * length, y_max = 0.5 * length;

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);

        const uint n_snap = partic[t].get_n_snap();

        meta_particle* snap = partic[t].get_snap_pointer();

        for (uint j = 0; j < n_snap; ++j)
        {
            // rotate by phi around the z-axis

            kpc x = cp * snap[j].r[0] + sp * snap[j].r[1];
            kpc y = -sp * snap[j].r[0] + cp * snap[j].r[1];
            const kpc z = snap[j].r[2];
            const float age = snap[j].age;

            const float vx = cp * snap[j].v[0] + sp * snap[j].v[1];
            const float vz = snap[j].v[2];

            // const float m = snap[j].m;

            // rotate by theta around the new y-axis

            kpc x_new = ct * x - st * z;

            const float vz_new = st * vx + ct * vz;

            x = x_new;

            // rotate by -alpha around the new z-axis

            x_new = ca * x + sa * y;
            kpc y_new = -sa * x + ca * y;

            // identify which particles appear in the slit
            const bool in_x = ( (x_new > x_min) && (x_new < x_max) );
            const bool in_y = ( (y_new > y_min) && (y_new < y_max) );

            bool in_age = true;

            if ( (age_bin[1] - age_bin[0]) > 1e-5 )
            {
                in_age = ( (age > age_bin[0]) && (age < age_bin[1]) );
            }

            if (in_x && in_y && in_age)
            {
                weighted_los_vel.push_back(vz_new);
            }
        }
    }

    sort(weighted_los_vel.begin(), weighted_los_vel.end());

    for (const float& v : weighted_los_vel)
    {
        cout << v END;
    }

} // particle_type_group::losvd()


void  particle_type_group::init(types _my_types, gadget_io* _gal)
{
    gal = _gal;

    n_particles = 0;

    N_projections = NP;

    my_types.gas = _my_types.gas;
    my_types.dm = _my_types.dm;
    my_types.disk = _my_types.disk;
    my_types.bulge = _my_types.bulge;
    my_types.stars = _my_types.stars;
    my_types.bh = _my_types.bh;

    number_of_types  = my_types.gas + my_types.dm + my_types.disk;
    number_of_types += my_types.bulge + my_types.stars + my_types.bh;

    max_sigma = mean_sigma = min_sigma = dev_sigma = 0;

    theta = phi = width = length = alpha =  0;

    age_cut = 0;

    age_bin[0] = age_bin[1] = 0;

    which_types = new pcat[number_of_types];

    uint k = 0;

    if (my_types.gas)
    {
        which_types[k] = pcat::Gas;
        ++k;
    }

    if (my_types.dm)
    {
        which_types[k] = pcat::DarkMatter;
        ++k;
    }

    if (my_types.disk)
    {
        which_types[k] = pcat::DiskStar;
        ++k;
    }

    if (my_types.bulge)
    {
        which_types[k] = pcat::BulgeStar;
        ++k;
    }

    if (my_types.stars)
    {
        which_types[k] = pcat::NewStar;
        ++k;
    }

    if (my_types.bh)
    {
        which_types[k] = pcat::BlackHole;
        ++k;
    }

    partic = new particle_type[6];

    for (uint i = 0; i < 6; ++i)
    {
        partic[i].init(i, gal);
    }

    for (uint i = 0; i < number_of_types; ++i)
    {
        const uint t = uint(which_types[i]);
        n_particles += gal->header.npart[t];
    }

    mas_pos = new float[4 * n_particles];

    update_mas_pos();

    BH_separation = BH_rel_speed = 0;

    using_rect_mesh = false;
    initialized = true;

} // particle_type_group::init()


void particle_type_group::free()
{
    if (initialized)
    {
        delete [] partic;
        partic = 0;

        delete [] which_types;
        which_types = 0;

        delete [] mas_pos;
        mas_pos = 0;
    }
} // particle_type_group::free()


void particle_type_group::find_bh_accretion()
{
    // at the moment, we assume that at most two black hole particles are
    // present in the snapshot file.  This should be generalized.

    static float _BH_accretion[4];

    // the mass and accretion rate of the first black hole
    _BH_accretion[0] = gal->Mbh[0];
    _BH_accretion[1] = gal->Mdot_bh[0];

    //the mass and accretion rate of the second black hole (if present);
    if (gal->header.npart[5] > 1)
    {
        _BH_accretion[2] = gal->Mbh[1];
        _BH_accretion[3] = gal->Mdot_bh[1];
    }
    else
    {
        _BH_accretion[2] = 0;
        _BH_accretion[3] = 0;
    }

    BH_accretion = _BH_accretion;
} // particle_type_group::find_bh_accretion()


void particle_type_group::set_particle_mass(const pcat cat,
        const uint n,
        const float m)
{
    partic[uint(cat)].set_particle_mass(n, m);
} // particle_type_group::set_particle_mass()


void particle_type_group::set_particle_metalicity(const pcat cat,
        const uint n,
        const float z)
{
    partic[uint(cat)].set_particle_metalicity(n, z);
} // particle_type_group::set_particle_metalicity(


void particle_type_group::set_particle_time(const uint n, const float timestamp)
{
    // only particle_category::NewStar (type 4) particles have creation times:
    partic[uint(pcat::NewStar)].set_particle_mass(n, timestamp);
} // particle_type_group::set_particle_time(


void particle_type_group::projection_statistics(const kpc _width,
        const kpc _length)
{
    float* velocity_dispersions = new float [N_projections];

    set_slit(_width, _length);

    for (uint i = 0; i < N_projections; ++i)
    {
        //choose uniform angular distribution for viewing angles

        theta = HALF_PI + asin(2.0 * gsnap::randnum() - 1.0);
        phi = TWO_PI * gsnap::randnum();

        //choose uniform angular distribution of slit position angles

        alpha = PI * gsnap::randnum();

        set_slit(_width, _length, alpha);

        set_direction(theta, phi);

        velocity_dispersions[i] = sigma_fast();
    }

    max_sigma = 0;
    min_sigma = 1e10;
    mean_sigma = 0;
    median_sigma = 0;
    dev_sigma = 0;

    float mean_square_sigma = 0;

    for (uint i = 0; i < N_projections; ++i)
    {
        const float vsi = velocity_dispersions[i];
        max_sigma = max(max_sigma, vsi);
        min_sigma = min(min_sigma, vsi);
        mean_sigma += vsi;
        mean_square_sigma += (vsi * vsi);
    }

    mean_sigma /= float(N_projections);
    mean_square_sigma /= float(N_projections);
    dev_sigma = sqrt(mean_square_sigma - mean_sigma * mean_sigma);

    std::sort(velocity_dispersions, velocity_dispersions + N_projections);

    median_sigma = velocity_dispersions[N_projections / 2];

    delete [] velocity_dispersions;

    find_bh_accretion();

    find_bh_separation();

    find_bh_rel_speed();

    // format the output so that the auto_gsnap.py script can use it.

    cout.precision(8);
    cout.setf(ios::fixed, ios::floatfield);
    cout << gal->header.time TAB \
         mean_sigma       TAB \
         median_sigma     TAB \
         max_sigma        TAB \
         min_sigma        TAB \
         dev_sigma        TAB \
         BH_accretion[0]  TAB \
         BH_accretion[1]  TAB \
         BH_accretion[2]  TAB \
         BH_accretion[3]  TAB \
         BH_separation    TAB \
         BH_rel_speed     END;
} // particle_type_group::projection_statistics(


void particle_type_group::sigma_probability_histogram()
{
    float* velocity_dispersions = new float [N_projections];


    srand(time(NULL));

    if (N_projections < 1000)
    {
        string msg = "In order to produce reliable statistics, the number of";
        msg += " projections must be at least 1000. You specified only ";
        msg += std::to_string(N_projections);
        gsnap::print_error(msg, err::syntax);
    }

    for (uint i = 0; i < N_projections; ++i)
    {
        theta = HALF_PI + asin(2.0 * gsnap::randnum() - 1.0);
        phi = TWO_PI * gsnap::randnum();
        alpha = PI * gsnap::randnum();
        velocity_dispersions[i] = sigma_fast();
        cout << theta TAB phi TAB velocity_dispersions[i] END;
    }

    float max_sigma = 0, min_sigma = 1e10;

    for (uint i = 0; i < N_projections; ++i)
    {
        const float vsi = velocity_dispersions[i];
        max_sigma = max(max_sigma, vsi);
        min_sigma = min(min_sigma, vsi);
    }

    std::sort(velocity_dispersions, velocity_dispersions + N_projections);

    const int n_intervals = floor(0.01 * N_projections);

    const float interval_width = (max_sigma - min_sigma) / n_intervals;

    float high_end = min_sigma + interval_width;

    int v_idx = 0;

    cout.precision(8);
    cout.setf(ios::fixed, ios::floatfield);
    cout << min_sigma TAB 0 END;

    for (int i = 0; i < (n_intervals - 1); ++i)
    {
        uint in_range = 0;

        while (velocity_dispersions[v_idx] < high_end )
        {
            ++in_range;
            ++v_idx;

            if (in_range == N_projections)
            {
                gsnap::print_warning("Problem building histogram!\n");
                break;
            }
        }

        in_range = 0;
        high_end += interval_width;
    }

    delete [] velocity_dispersions;
} // particle_type_group::sigma_probability_histogram()


void particle_type_group::rect_mesh(pixel_array* canvas)
{
    using_rect_mesh = true;

    float m, x, y, z, x_temp, y_temp;
    bool in_x, in_y;

    const float ct = cos(theta), st = sin(theta);
    const float cp = cos(phi), sp = sin(phi);
    const float x_max = 0.5 * canvas->view_size;
    const float y_max = canvas->aspect_ratio * x_max;
    const float x_min = -x_max;
    const float y_min = -y_max;

    // loop over included particles. Compare their x and y coordinates to the
    // limits of the bounding box. Then map each particle to an array element.

    #pragma omp parallel for private( x_temp, y_temp, x, y, z, m, in_x, in_y )

    for (int i = 0; (uint)i < n_particles; ++i)
    {
        x = mas_pos[4 * i];
        y = mas_pos[4 * i + 1];
        z = mas_pos[4 * i + 2];
        m = mas_pos[4 * i + 3];

        // rotate by \phi around the z-axis

        x_temp = cp * x + sp * y;
        y_temp = -sp * x + cp * y;

        // rotate by \theta around the new y-axis

        x = ct * x_temp - st * z;
        y = -y_temp;

        in_x = (x > x_min && x < x_max);
        in_y = (y > y_min && y < y_max);

        if (in_x && in_y)
        {
            uint x_index, y_index;
            uint x_res = canvas->x_res;
            uint y_res = canvas->y_res;
            uint x_shift = (uint)((float)x_res * 0.5);
            uint y_shift = (uint)((float)y_res * 0.5);

            // Identify indices

            x_index = x_shift + floor(x * canvas->ipixel_size);

            y_index = y_shift + floor(y * canvas->ipixel_size);

            // add the particle's mass to the pixel array

            canvas->pixels[x_res * y_index + x_index] += m;
        }
    }
} // particle_type_group::rect_mesh()


void particle_type_group::update_mas_pos()
{
    uint n_snap, t, k = 0;
    meta_particle* snap;

    // loop over included particles and transfer the positions
    // and masses to a C array.

    for (uint i = 0; i < number_of_types; ++i)
    {
        t = uint(which_types[i]);

        n_snap = partic[t].get_n_snap();

        snap = partic[t].get_snap_pointer();

        for (uint j = 0; j < n_snap; ++j)
        {
            bool in_age = true;

            const float age = snap[j].age;

            if (age_bin[1] - age_bin[0])
            {
                in_age = ( (age > age_bin[0]) && (age < age_bin[1]) );
            }

            if (in_age)
            {
                mas_pos[k++] = snap[j].r[0];
                mas_pos[k++] = snap[j].r[1];
                mas_pos[k++] = snap[j].r[2];
                mas_pos[k++] = snap[j].m;
            }
            else
            {
                mas_pos[k++] = 0;
                mas_pos[k++] = 0;
                mas_pos[k++] = 0;
                mas_pos[k++] = 0;
            }

        }
    }
} // particle_type_group::update_mas_pos()


void particle_type_group::to_voxel_array(voxel_array* volume)
{
    /*! \todo fix the duplicate code problem in this function. The perspective
         and orthographic projections largely use the same code. The duplicate
         code can be removed from the conditional statements. */
    compact_sph gas;
    color_particle star;

    float x, y, z;
    bool in_x, in_y, in_z, sph_type;

    const float x_max = 0.5 * (volume->view_size);
    const float y_max = volume->aspect_ratio * x_max;
    const float z_max = 0.5 * (volume->view_depth);
    const float z_min = -z_max;
    const float x_min = -x_max;
    const float y_min = -y_max;

    uint n_snap;
    pcat t;
    meta_particle* snap;

    float D = volume->camera_dist;

    for (uint i = 0; i < number_of_types; ++i)
    {
        t = which_types[i];

        sph_type = (t == pcat::Gas);

        bool new_star = (t == pcat::NewStar);

        n_snap = partic[uint(t)].get_n_snap();

        snap = partic[uint(t)].get_snap_pointer();

        for (uint j = 0; j < n_snap; ++j)
        {
            if (volume->perspective)
            {
                // transform positions and masses for perspective projection.

                float s = D / (D - snap[j].r[2]);

                if (sph_type) // this is a gas particle
                {
                    gas.r[0] = x = s * snap[j].r[0];
                    gas.r[1] = y = s * -snap[j].r[1];
                    gas.r[2] = z = snap[j].r[2];
                    gas.m = s * s * snap[j].m;
                    gas.met = snap[j].metz;
                    gas.u = snap[j].u;
                }
                else // this is a stellar particle
                {
                    star.r[0] = x = s * snap[j].r[0];
                    star.r[1] = y = s * -snap[j].r[1];
                    star.r[2] = z = snap[j].r[2];

                    float age = gal->header.time - snap[j].age;
                    float red_boost = 1.0;
                    float blue_boost = 1.0;

                    // perform mass adjustment to make younger stars brighter
                    // and bluer.

                    if (new_star) // don't do this for disk and bulge stars
                    {
                        if (age < 0.008)
                        {
                            blue_boost = 210;
                            red_boost = 60;
                        }
                        else if (age < 0.15)
                        {
                            blue_boost = 16;
                            red_boost = 8;
                        }
                        else if (age < 0.25)
                        {
                            blue_boost = 8;
                            red_boost = 5;
                        }
                    }

                    star.blue = s * s * snap[j].m * blue_boost;
                    star.red = s * s * snap[j].m * red_boost;
                }
            }
            else // we are not performing perspective projection
            {
                if (sph_type) // this is a gas particle
                {
                    gas.r[0] = x = snap[j].r[0];
                    gas.r[1] = y = -snap[j].r[1];
                    gas.r[2] = z = snap[j].r[2];
                    gas.m = snap[j].m;
                    gas.met = snap[j].metz;
                    gas.u = snap[j].u;
                }
                else // this is a stellar particle
                {
                    star.r[0] = x = snap[j].r[0];
                    star.r[1] = y = -snap[j].r[1];
                    star.r[2] = z = snap[j].r[2];

                    const float age = gal->header.time - snap[j].age;
                    float red_boost = 1.0;
                    float blue_boost = 1.0;

                    // perform mass adjustment to make younger stars brighter
                    // and bluer.

                    if (new_star) // don't do this for disk and bulge stars
                    {
                        if (age < 0.008)
                        {
                            blue_boost = 210;
                            red_boost = 60;
                        }
                        else if (age < 0.15)
                        {
                            blue_boost = 16;
                            red_boost = 8;
                        }
                        else if (age < 0.25)
                        {
                            blue_boost = 8;
                            red_boost = 5;
                        }
                    }

                    star.blue = snap[j].m * blue_boost;
                    star.red = snap[j].m * red_boost;
                }
            }

            in_x = (x > x_min && x < x_max);
            in_y = (y > y_min && y < y_max);
            in_z = (z > z_min && z < z_max);

            if (in_x && in_y && in_z)
            {
                if (sph_type)
                {
                    volume->add_particle(gas);
                }
                else
                {
                    volume->add_particle(star);
                }
            }
        }
    }
} // particle_type_group::to_voxel_array()

