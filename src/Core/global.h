/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
  \brief Short global enumerations, data types, and functions.

  This header in meant to be included in all other header files that are part of
  the GSnap project. Basic data types and preprocessor directives are defined
  here and the most basic functions are declared here. The functions are in the
  gsnap namespace. In practice, this header does not have to be explicitly
  included in all header files because global.h includes this header.

 ******************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

#include "settings.h"

#include <string>

////////////////////// Pre-processor macro definitions /////////////////////////

#define PI 3.14159265358979323846
#define TWO_PI 6.28318530717958647692
#define HALF_PI 1.57079632679489661923
#define TAB << "\t" <<
#define COMMA << ", " <<
#define END << std::endl

///////////////////////////////// Enumerations /////////////////////////////////

/*! Specifies the simulation snapshot format. */
enum class snap_format
{
    none,     /*!< Unknown file format. */
    Gadget,   /*!< The GADGET-2 or GADGET-2 type 1 file format. */
    Gadget2,  /*!< The GADGET-2, type 1 file format. */
    Gadget3   /*!< The GADGET-3, type 1 file format. */
};

/*! Specifies the particle category, otherwise known as the particle "type." */
enum class pcat : uint
{
    Gas,        /*!< Gas (SPH) particles. */
    DarkMatter, /*!< Dark Matter particles. */
    DiskStar,   /*!< Stars originally located in the stellar disk. */
    BulgeStar,  /*!< Stars originally located in the stellar bulge.*/
    NewStar,    /*!< Stars that formed from gas during the simulation. */
    BlackHole   /*!< Black hole "sink" particles. */
};

/*! Specifies the type of error that has occurred */
enum class err : uint
{
    syntax,   /*!< The user made a syntax error when writing a command. */
    io,       /*!< Input/output error. */
    bug       /*!< A coding bug that should be fixed by GSnap's developers. */
};

////////////////////////////// Elementary classes //////////////////////////////

/*!
 * \brief The rgb class is a simple color class storing a red, green, blue
 * triplet of subpixel values in an arbitrary data type (float, char, short, and
 * int are typical types for T).
 */
template<typename T>
class rgb
{
protected:

    T red_;
    T green_;
    T blue_;

public:

    T red() const { return red_; }
    T green() const { return green_; }
    T blue() const { return blue_; }

    void operator()(const T _red, const T _green, const T _blue)
    {
        red_   = _red;
        green_ = _green;
        blue_  = _blue;
    }
};

/////////////////////////////// type definitions ///////////////////////////////
// It would be nice if these were strong typedefs. Since the language does not
// support strong typedefs yet, a class (struct) would be required to maintain
// type safety.

/*! unsigned integer (at least 32 bits). */
typedef unsigned int uint;

/*! unsigned long long integer (at least 64 bits). */
typedef unsigned long long ullint;

/*! unit of distance: kiloparsecs. */
typedef float kpc;

/*! unit of length: pixel width */
typedef unsigned int pix;

/*! angular unit: radians. */
typedef float rad;

///////////////////////////// User-defined literals ////////////////////////////

/*! A user-defined literal for the kpc type. Using this, it is possible to
    quickly specify the units that are being used. For instance, 100.0_kpc is
    interpreted as a number of type kpc. This form handles floating point
    literals.*/
constexpr kpc operator"" _kpc(long double d)
{
    return static_cast<kpc>(d);
}

/*! A user-defined literal for the kpc type. This form handles integer
    literals.*/
constexpr kpc operator"" _kpc(unsigned long long d)
{
    return static_cast<kpc>(d);
}

/*! A user-defined literal for the pix type.*/
constexpr pix operator"" _pix(unsigned long long d)
{
    return static_cast<pix>(d);
}

/*! A user-defined literal for the rad type.*/
constexpr rad operator"" _rad(unsigned long long d)
{
    return static_cast<rad>(d);
}

/*! A user-defined literal for the rad type.*/
constexpr rad operator"" _rad(long double d)
{
    return static_cast<rad>(d);
}

/*! A user-defined literal that converts degrees to radians.*/
constexpr rad operator"" _deg(long double d)
{
    return 0.017453292519943 * d;
}

/*! A user-defined literal that converts degrees to radians.*/
constexpr rad operator"" _deg(unsigned long long d)
{
    return 0.017453292519943 * d;
}

//////////////////////////////////// Functions /////////////////////////////////

/*! The gsnap namespace contains all global (project-wide) helper functions.*/
namespace gsnap
{

/*! Draws attention to text in a terminal / console by displaying the text in
    bold red font.
    \param[in] text A string that will be displayed in red to grab attention in
          a terminal / window / console.
    \param[in] msg A message that follows the string. This is generally an
           informative message to the user. */
void attention(const char* text,
               const std::string& msg) __attribute__((cold));

/*! Checks whether file is a png image. */
bool is_PNG(const char* file) __attribute__((cold));

/*! Tests whether the argument string represents a numeric type. Returns
    true only if the string pointed to by str represents a numerical value.*/
bool is_numeric(const char* str) __attribute__((cold));

/*! Prints the warning message std::string, msg.*/
void print_warning(const std::string& msg) __attribute__((cold));

/*! Prints the warning message C-string, _msg.*/
void print_warning(const char* _msg) __attribute__((cold));

/*! Prints the error message string and exits with the specified status*/
void print_error(const std::string& msg,
                 err status) __attribute__ ((noreturn, cold));

/*! Prints the error message string and exits with the specified status*/
void print_error(const char* _msg,
                 err status) __attribute__ ((noreturn, cold));

/*! Generate a uniform random number from 0 to 1, inclusive. This function seeds
 *  itself based upon the wall-clock time when it was first called.*/
float randnum();

} // ns gsnap

#endif // GLOBAL_H
