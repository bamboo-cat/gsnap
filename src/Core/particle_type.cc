/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Contains the particle_type class member functions. */

#include "Core/particle_type.h"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <ctime>
#include <omp.h>

using namespace std;

particle_type::particle_type(pcat tid, gadget_io* _gal)
{
    init(tid, _gal);
} // particle_type::particle_type(tid)


particle_type::particle_type() { }


void particle_type::init(pcat tid, gadget_io* _gal)
{
    gal = _gal;

    type_id = tid;

    n_snap = gal->header.npart[uint(tid)];

    free();

    if (!initialized)
    {
        snap.reserve(n_snap);
        populate_snap();
        initialized = true;
    }
} // particle_type::init()


void particle_type::init(uint tid, gadget_io* _gal)
{
    init(static_cast<pcat>(tid), _gal);
}


void particle_type::update_gal()
{
    uint nr_i, n_min = 0;

    // determine n_min: the value of the first particle of type, type_id.
    if (uint(type_id) > 0)
    {
        for (uint i = 0; i < uint(type_id); ++i)
        {
            n_min += gal->header.npart[i];
        }
    }

    for (uint i = 0; i < n_snap; ++i)
    {
        nr_i = n_min + i;

        // update positions

        gal->pos[3 * nr_i]   = snap[i].r[0];
        gal->pos[3 * nr_i + 1] = snap[i].r[1];
        gal->pos[3 * nr_i + 2] = snap[i].r[2];

        // update velocities

        gal->vel[3 * nr_i]   = snap[i].v[0];
        gal->vel[3 * nr_i + 1] = snap[i].v[1];
        gal->vel[3 * nr_i + 2] = snap[i].v[2];

        // update masses and ID numbers

        gal->m[nr_i] = snap[i].m;
        gal->id[nr_i] = snap[i].id;

        // update sph quantities

        if (type_id == pcat::Gas)
        {
            gal->u[nr_i] = snap[i].u;
            gal->rho[nr_i] = snap[i].rho;
            gal->ne[nr_i] = snap[i].ne;
            gal->nh[nr_i] = snap[i].nh;
            gal->hsml[nr_i] = snap[i].hsml;
            gal->sfr[nr_i] = snap[i].sfr;
        }

        // update ages

        if (type_id == pcat::NewStar)
        {
            gal->age[i] = snap[i].age;
        }

        // update metalicities

        if (type_id == pcat::Gas)
        {
            gal->metz[nr_i] = snap[i].metz;
        }
        else if (type_id == pcat::NewStar)
        {
            gal->metz[gal->header.npart[0] + i] = snap[i].metz;
        }

        // update potentials

        gal->pot[nr_i] = snap[i].pot;
    }
} // particle_type::update_gal()


void particle_type::renew_snapshot()
{
    for (uint i = 0; i < n_snap; ++i)
    {
        // add positions

        gal->pos.push_back(snap[i].r[0]);
        gal->pos.push_back(snap[i].r[1]);
        gal->pos.push_back(snap[i].r[2]);

        // update velocities

        gal->vel.push_back(snap[i].v[0]);
        gal->vel.push_back(snap[i].v[1]);
        gal->vel.push_back(snap[i].v[2]);

        // update masses and ID numbers

        gal->m.push_back(snap[i].m);
        gal->id.push_back(snap[i].id);

        // update sph quantities

        if (type_id == pcat::Gas)
        {
            gal->u.push_back(snap[i].u);
            gal->rho.push_back(snap[i].rho);
            gal->ne.push_back(snap[i].ne);
            gal->nh.push_back(snap[i].nh);
            gal->hsml.push_back(snap[i].hsml);
            gal->sfr.push_back(snap[i].sfr);
        }

        // update ages

        if (type_id == pcat::NewStar)
        {
            gal->age.push_back(snap[i].age);
        }

        // update metalicities

        if ( (type_id == pcat::Gas) || (type_id == pcat::NewStar) )
        {
            gal->metz.push_back(snap[i].metz);
        }

        // update potentials

        gal->pot.push_back(snap[i].pot);

        // update black hole masses

        if (type_id == pcat::BlackHole)
        {
            gal->Mbh.push_back(snap[i].m);
        }
    }
} // particle_type::renew_snapshot()


void particle_type::populate_snap()
{
    uint nr_i, n_min = 0;

    if (uint(type_id) > 0)
    {
        for (uint i = 0; i < uint(type_id); ++i)
        {
            n_min += gal->header.npart[i];
        }
    }

    meta_particle blank;

    for (uint i = 0; i < n_snap; ++i)
    {
        nr_i = n_min + i;

        snap.push_back(blank);

        // assign positions

        snap[i].r[0] = gal->pos[3 * nr_i];
        snap[i].r[1] = gal->pos[3 * nr_i + 1];
        snap[i].r[2] = gal->pos[3 * nr_i + 2];

        // assign velocities

        snap[i].v[0] = gal->vel[3 * nr_i];
        snap[i].v[1] = gal->vel[3 * nr_i + 1];
        snap[i].v[2] = gal->vel[3 * nr_i + 2];

        // assign particle masses and ID numbers

        snap[i].m = gal->m[nr_i];
        snap[i].id = gal->id[nr_i];

        // assign SPH quantities

        if (type_id == pcat::Gas)
        {
            snap[i].u = gal->u[nr_i];
            snap[i].rho = gal->rho[nr_i];
            snap[i].ne = gal->ne[nr_i];
            snap[i].nh = gal->nh[nr_i];
            snap[i].hsml = gal->hsml[nr_i];
            snap[i].sfr = gal->sfr[nr_i];
        }
        else
        {
            snap[i].u = 0;
            snap[i].rho = 0;
            snap[i].ne = 0;
            snap[i].nh = 0;
            snap[i].hsml = 0;
            snap[i].sfr = 0;
        }

        // assign "ages" (these are actually creation times).

        if (type_id == pcat::NewStar)
        {
            snap[i].age = gal->age[i];
        }
        else
        {
            snap[i].age = 0;
        }

        // assign metalicities

        if (type_id == pcat::Gas)
        {
            snap[i].metz = gal->metz[nr_i];
        }
        else if (type_id == pcat::NewStar)
        {
            snap[i].metz = gal->metz[gal->header.npart[0] + i];
        }
        else
        {
            snap[i].metz = 0;
        }

        // assign potentials

        snap[i].pot = gal->pot[nr_i];
    }
} // particle_type::populate_snap()


void particle_type::compute_cm(float* cm)
{
    float mi;
    float x = 0, y = 0, z = 0, vx = 0, vy = 0, vz = 0, mass = 0;

    for (uint i = 0; i < n_snap; ++i)
    {
        mi = snap[i].m;
        x += snap[i].r[0] * mi;
        y += snap[i].r[1] * mi;
        z += snap[i].r[2] * mi;
        vx += snap[i].v[0] * mi;
        vy += snap[i].v[1] * mi;
        vz += snap[i].v[2] * mi;
        mass += mi;
    }

    mi = 1.0 / mass;

    cm[0] = mass;
    cm[1] = x * mi;
    cm[2] = y * mi;
    cm[3] = z * mi;
    cm[4] = vx * mi;
    cm[5] = vy * mi;
    cm[6] = vz * mi;

} // particle_type::compute_cm()


bool particle_type::find_bh(const uint which_bh, float* cm)
{
    if (n_snap >= (which_bh + 1))
    {
        cm[1] = snap[which_bh].r[0];
        cm[2] = snap[which_bh].r[1];
        cm[3] = snap[which_bh].r[2];
        cm[4] = snap[which_bh].v[0];
        cm[5] = snap[which_bh].v[1];
        cm[6] = snap[which_bh].v[2];

        return true;
    }
    else
    {
        return false;
    }
}


void particle_type::set_particle_mass(uint n, float m)
{
    if (n < n_snap)
    {
        snap[n].m = m;
    }
}


void particle_type::set_particle_metalicity(uint n, float z)
{
    if (n < n_snap)
    {
        snap[n].metz = z;
    }
}


void particle_type::set_particle_time(uint n, float timestamp)
{
    if ( (n < n_snap) && (type_id == pcat::NewStar) )
    {
        snap[n].age = timestamp;
    }
}


void particle_type::shift(const float* cm)
{

    for (uint i = 0; i < n_snap; ++i)
    {
        snap[i].r[0] -= cm[1];
        snap[i].r[1] -= cm[2];
        snap[i].r[2] -= cm[3];
        snap[i].v[0] -= cm[4];
        snap[i].v[1] -= cm[5];
        snap[i].v[2] -= cm[6];
    }

} // particle_type::shift()


void particle_type::rotate(const rad _theta, const rad _phi)
{
    theta = _theta;
    phi = _phi;

    float ct = cos(theta), st = sin(theta);
    float cp = cos(phi), sp = sin(phi);
    float x, y, z, vx, vy, vz;

    for (uint i = 0; i < n_snap; ++i)
    {
        // rotate by \phi around the z-axis

        x = cp * snap[i].r[0] + sp * snap[i].r[1];
        y = -sp * snap[i].r[0] + cp * snap[i].r[1];
        z = snap[i].r[2];

        vx = cp * snap[i].v[0] + sp * snap[i].v[1];
        vy = -sp * snap[i].v[0] + cp * snap[i].v[1];
        vz = snap[i].v[2];

        // rotate by \theta around the new y-axis

        snap[i].r[0] = ct * x - st * z;
        snap[i].r[1] = y;
        snap[i].r[2] = st * x + ct * z;

        snap[i].v[0] = ct * vx - st * vz;
        snap[i].v[1] = vy;
        snap[i].v[2] = st * vx + ct * vz;
    }
} // particle_type::rotate()


uint particle_type::v_los(const uint first, float* v_los_array)
{
    mass_in_slit = 0;

    uint pos = first;
    const float x_min = -0.5 * width, x_max = 0.5 * width;
    const float y_min = -0.5 * length, y_max = 0.5 * length;
    const float ca = cos(alpha), sa = sin(alpha);
    float x, y, x_new, y_new;
    bool in_x, in_y;

    if ( (1000 * width == 0) || (1000 * length == 0) )
    {
        string msg = "You have not specified the slit dimensions.\n"
                     "Use particle_type::set_slit(width, length, alpha).";
        gsnap::print_error(msg, err::bug);
    }

    for (uint i = 0; i < n_snap; ++i)
    {
        x = snap[i].r[0];
        y = snap[i].r[1];

        // rotate system by -alpha around the new z-axis

        x_new = ca * x + sa * y;
        y_new = -sa * x + ca * y;

        // identify which particles appear in the slit

        in_x = ( (x_new > x_min) && (x_new < x_max) );
        in_y = ( (y_new > y_min) && (y_new < y_max) );

        if (in_x && in_y)
        {
            v_los_array[2 * pos] = snap[i].v[2];
            v_los_array[2 * pos + 1] = snap[i].m;

            mass_in_slit += snap[i].m;
            ++pos;
        }
    }

    number_in_slit = pos - first;

    return pos;

} // particle_type::v_los()

