/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides tools for storing and manipulating particle data.

   Tools for storing and manipulating GADGET-2 Snapshot particle data. The
   classes defined in this file form the core of GSnap. All other functions
   and classes use the classes defined here in some way. See particles.cc for
   member function definitions.

   \todo Fix the behavior of the --speed option and the speed column of the
   sigma_stats function.
*/

#ifndef PARTICLES_H
#define PARTICLES_H

#include "IO/io.h"
#include "Core/basic_particle.h"
#include "Core/particle_type.h"
#include "Core/global.h"

// forward declarations
class  pixel_array;
class  voxel_array;


/*! \brief Performs computations and manipulations for a group of particle_type
    objects.

    A class for handling multiple types of particles simultaneously.
    This is useful if, for instance, we are interested in simultaneously
    analyzing or manipulating all stellar particles (types 2, 3, and 4) or
    dark matter and black holes (types 1 and 5), but we are uninterested
    in the other particle types. It can also be used to operate on all
    particle types simultaneously. */
class particle_type_group
{

protected:

    particle_type*  partic;
    gadget_io*       gal;
    types           my_types;
    uint            n_particles;
    pcat*           which_types;
    uint            number_of_types;
    uint            number_in_slit;
    uint            N_projections;
    rad             theta;
    rad             phi;
    rad             alpha;
    kpc             width;
    kpc             length;
    float           mean_sigma;
    float           max_sigma;
    float           min_sigma;
    float           median_sigma;
    float           dev_sigma;
    float*          mas_pos;
    float*          BH_accretion;
    float           age_cut;
    float           age_bin[2];
    kpc             BH_separation;
    float           BH_rel_speed;
    bool            initialized;
    bool            using_rect_mesh;

public:

    /*! parameterized, immediate constructor */
    particle_type_group(const types my_types, gadget_io* _gal);

    /*! the deferred initialization constructor: do not do anything yet. Wait
        for the init() member to be called */
    particle_type_group()
    {
        initialized = false;
    }

    ~particle_type_group() { free(); }

    /*! Initialize / allocate memory for the object */
    void init(types _my_types, gadget_io* _gal);

    /*! free the allocated memory */
    void free();

    /*! Compute the global center of mass coordinates and velocity of the
        center of mass */
    void compute_cm(float* cm);

    /*! Get the coordinates of the black hole with index which_bh. */
    bool find_bh(const uint which_bh, float* cm);

    /*! Set the mass of the nth particle of type particle_type_id to m. */
    void set_particle_mass(const pcat cat,
                           const uint n,
                           const float m);

    /*! Set the metalicity of the nth particle of type particle_type_id to z. */
    void set_particle_metalicity(const pcat cat,
                                 const uint n,
                                 const float z);

    /*! Set the creation time of the nth "new" particle to timestamp. */
    void set_particle_time(const uint n, const float timestamp);

    /*! Shift the positions and velocities of all included particle types. */
    void shift(const float* cm);

    /*! rotate the particle positions and velocities such that the new z-axis
        points in the direction of the spherical coordinate theta, phi. */
    void rotate(const rad _theta, const rad _phi);

    /*! copy particle information from partic[] back to galaxy_data structure */
    void update_gal();

    /*! Clear the contents of the gal snapshot and save the contents of this
        object to gal rather than calling update_gal(), which only modifies
        the gal snapshot in-place. The resulting snapshot can have a different
        number of particles than the original snapshot; this is not possible
        with the update_gal() method.*/
    void renew_snapshot();

    /*! set the age cutoff variable age_cut. Particles older than a certain age
        will be treated differently than particles younger than this age. The
        variable age_cut tells us what the cutoff creation time is. If age_cut
        is positive, the velocity dispersion of particles created before
        abs(age_cut) will be computed. If age_cut is negative, the velocity
        dispersion particles older than age_cut will be computed. If age_cut=0,
        the velocity dispersion of all stars will be computed. */
    void set_age_cutoff(const float _age_cut) { age_cut = _age_cut; }

    void set_age_bin(const float* b) {age_bin[0] = b[0]; age_bin[1] = b[1];}

    /*! Set slit dimensions and position angle */
    void set_slit(const kpc _width,
                  const kpc _length,
                  const rad _alpha = 0);

    /*! set direction angles (alpha, phi), but do not actually rotate
        the particles. */
    void set_direction(const rad _t, const rad _p) { theta = _t; phi = _p; }

    /*! compute the mass-weighted line-of-sight velocity dispersion of particles
        appearing within the slit and rotate the entire snapshot file. */
    float sigma();

    /*! compute the mass-weighted line-of-sight velocity dispersion of particles
        appearing within the slit ___without actually rotating the particles___
        This is much faster than the sigma() method because it only computes
        quantities that are absolutely necessary and it does not store updated
        positions and velocities. This could easily be multi-threaded so that
        the outer-most for loop runs in parallel.*/
    float sigma_fast();

    /*! Compute the line of sight velocity distribution histogram and output
    to std output. (Note: this is not finished and there is no way of specifying
    this option using a script or the command line interface.) */
    void losvd();

    /*! computes a probability histogram of velocity dispersion measurements.*/
    void sigma_probability_histogram();

    /*! Return the number of particles in the slit / mask */
    uint get_number_in_slit() const { return number_in_slit;  }

    /*! Determine the black hole mass(es) and accretion rate(s) */
    void find_bh_accretion();

    /*! Determine the separation distance between black hole 0 and black hole 1
        and store the result in BH_separation. If there is only one black hole,
        then the result is zero*/
    void find_bh_separation();

    /*! compute the relative speed of BH_1 with respect to BH_2*/
    void find_bh_rel_speed();

    /*! return the relative black hole speed. */
    float get_bh_rel_speed() const;

    /*! compute the mean, max, min, and standard deviation of sigma along NP
        viewing directions __without actually rotating the system__*/
    void projection_statistics(const kpc _width, const kpc _length);

    /*! Set the number of projections used in the projection statistics.*/
    void set_N_projections(const uint N) { N_projections = N; }

    /*! Output a 2-D, rectangular (x_res x y_res) array which lists the number
        of particles in each element of a Cartesian mesh (like a pixel array)
        projected along the z-axis. The mesh is centered at the current
        origin (x=0, y=0) and extends the in x and y directions, such that:

                         -box_size/2 < x,y < box_size/2

       This array is useful for creating simple images of the system as well as
       for computing the projected half-mass radius of the system */
    void rect_mesh(pixel_array* canvas);

    /*! update the mas_pos array. This is a fast array for storing masses and
        positions of particles for imaging a interactive viewing purposes. This
        array does not need to be updated when particles are not being viewed
        and it may contain only a sub-sample of the particles in the system
        in order to improve the interactive viewing performance. To enable
        automatic updating of this array, call use_rect_mesh(true) */
    void update_mas_pos();

    /*! Specify whether we are automatically updating the mas_pos array.*/
    void use_rect_mesh(const bool usm) { using_rect_mesh = usm; }

    /*! Transfer data from the particle group object to a voxel_array object.*/
    void to_voxel_array(voxel_array* volume);

    /*! Experimental: outputs the relative speed of the two black holes, sigma
        along the collision direction, and the time of the snapshot.*/
    void collide_on_z();

    /*! Add a particle, p, of species, s. */
    void push_back(meta_particle p, pcat s)
    {
        partic[uint(s)].push_back(p);
        ++n_particles;
    }

    /*! Remove a particle of species s from the end of the array. */
    void pop_back(pcat s)
    {
        partic[uint(s)].pop_back();
        --n_particles;
    }

    /*! Returns the number of particles of species, s. */
    uint get_n_type(pcat s) const
    {
        return partic[uint(s)].get_n_snap();
    }

    /*! Returns a pointer to the first element in the array of meta_particle
        objects for particles of species, s. */
    meta_particle* get_snap_pointer(pcat s) const
    {
        return partic[uint(s)].get_snap_pointer();
    }
};


#endif // PARTICLES_H
