/* This is a test of the depth_of_field class. It depends upon the behavior of
   and the interface of the voxel class, since the depth_of_field class is
   specifically designed for blurring a 2D slice of voxels. It would */


#include<Viz/depth_of_field.h>

#include <QtTest/QtTest>
#include <limits>

template<typename T>
bool FuzzyLessThan(T a, T b)
{
    return ( a - b < std::numeric_limits<T>::epsilon() );
}


class Test_depth_of_field: public QObject
{
    Q_OBJECT

private:

    pix x_res;
    pix y_res;
    uint n_pixels;
    float u;
    voxel vox;
    voxel* slice;

private slots:

    void initTestCase();     // runs once at the beginning of the unit test
    void init();             // runs before each individual test function
    void enabled();
    void blur_success();
    void blur_energy_conservation();
    void blur_total_content();
    void cleanup();          // runs after each individual test function
    void cleanupTestCase();  // runs at the end of the test unit test
 };


void Test_depth_of_field::initTestCase()
{
    x_res = 1000;
    y_res = 1000;
    n_pixels = x_res * y_res;
    u = 0.2;

    float red = 1.0, blue = 1.0, mass = 1.0;
    vox.set_color(blue, red);
    vox.set_mass(mass);
    vox.set_u(u);

    slice = new voxel[2 * x_res * y_res];
}


void Test_depth_of_field::init()
{
    for (uint i = 0; i < n_pixels; ++i) { slice[i].zero(); }

    int j_center = y_res/2;
    int i_center = x_res/2;
    int i_edge = x_res - 10;

    slice[y_res * j_center + i_center] = vox;
    slice[y_res * j_center + i_edge] = vox;
}


void Test_depth_of_field::cleanup() {}


void Test_depth_of_field::cleanupTestCase()
{
    delete [] slice;
}


void Test_depth_of_field::enabled()
{
    depth_of_field dof0;
    QCOMPARE(dof0.enabled(), false);

    depth_of_field dof1(0.5, 200, 100, 100);
    QCOMPARE(dof1.enabled(), false);

    depth_of_field dof2(0.33, 200, 100, 100);
    QCOMPARE(dof2.enabled(), true);

    depth_of_field dof3(0.9, 100, 100, 100);
    QCOMPARE(dof3.enabled(), false);
}


// dof.blur(...) should succeed in this sitution and, thus, return 1.
void Test_depth_of_field::blur_success()
{
    depth_of_field dof(0.2, 500, x_res, y_res);

    QCOMPARE(dof.blur(slice, 10), 1);
}


// this is a rather weak test that will only detect significant
// problems with the treatment of the internal energy variable u.
// Specifically, the temperature should certainly not increase while
// blurring. If this fails, it might also point to a defect in the
// voxel class.
void Test_depth_of_field::blur_energy_conservation()
{
    depth_of_field dof(0.2, 500, x_res, y_res);

    dof.blur(slice, 10);

    for (uint i = 0; i < n_pixels; ++i)
    {
        QVERIFY(FuzzyLessThan( slice[i].get_u(), u ) );
    }
}


// this test is sensitive to small changes in the code, since a small
// modification will lead to a different value for the total mass or light
// in the slice. It will not detect certain visual effects; these still need
// to be tested manually by looking at the image output.
void Test_depth_of_field::blur_total_content()
{
    depth_of_field dof(0.2, 500, x_res, y_res);

    dof.blur(slice, 10);

    float total_sum = 0;

    for (uint i = 0; i < n_pixels; ++i)
    {
        total_sum += slice[i].sum();
    }

    QCOMPARE(total_sum, 4.87774f); // The total sum should be 4.87774
}

 QTEST_MAIN(Test_depth_of_field)
 #include "test_depth_of_field.moc"
