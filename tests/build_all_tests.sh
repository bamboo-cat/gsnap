#! /bin/sh

cd depth_of_field && ./build.sh && cd ..

list_of_directories=$(ls -l | awk '/^d/ {print $9}')

for i in $list_of_directories; do 
    cd $i && ./build.sh && cd ..
done